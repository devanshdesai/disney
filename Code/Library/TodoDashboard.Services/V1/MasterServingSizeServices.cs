﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Data.Contract;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Services.V1
{
    public class MasterServingSizeServices : AbstractMasterServingSizeServices
    {
        private AbstractMasterServingSizeDao  abstractMasterServingSizeDao = null;

        public MasterServingSizeServices(AbstractMasterServingSizeDao abstractMasterServingSizeDao)
        {
            this.abstractMasterServingSizeDao=abstractMasterServingSizeDao;
        }

        public override PagedList<AbstractMasterServingSize> MasterServingSize_All(PageParam pageParam, string search)
        {
            return abstractMasterServingSizeDao.MasterServingSize_All(pageParam, search);
        }

        public override bool MasterServingSize_Delete(int Id)
        {
            return abstractMasterServingSizeDao.MasterServingSize_Delete(Id);
        }

        public override SuccessResult<AbstractMasterServingSize> MasterServingSize_Id(int Id)
        {
            return abstractMasterServingSizeDao.MasterServingSize_Id(Id);
        }

        public override SuccessResult<AbstractMasterServingSize> MasterServingSize_Upsert(AbstractMasterServingSize abstractMasterServingSize)
        {
            return abstractMasterServingSizeDao.MasterServingSize_Upsert(abstractMasterServingSize);
        }
    }
}
