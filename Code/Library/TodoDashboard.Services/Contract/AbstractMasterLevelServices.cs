﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;

namespace TodoDashboard.Services.Contract
{
    public abstract class AbstractMasterLevelServices
    {
        public abstract SuccessResult<AbstractMasterLevel> MasterLevel_Upsert(AbstractMasterLevel AbstractMasterLevel);

        public abstract SuccessResult<AbstractMasterLevel> MasterLevel_Id(int Id);

        public abstract PagedList<AbstractMasterLevel> MasterLevel_All(PageParam pageParam, string search, AbstractMasterLevel AbstractMasterLevel = null);

        public abstract SuccessResult<AbstractMasterLevel> MasterLevel_Delete(int Id);
    }
}
