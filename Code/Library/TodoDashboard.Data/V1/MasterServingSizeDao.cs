﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Data.Contract;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;

namespace TodoDashboard.Data.V1
{
    public class MasterServingSizeDao : AbstractMasterServingSizeDao
    {
        public override PagedList<AbstractMasterServingSize> MasterServingSize_All(PageParam pageParam, string search)
        {
            PagedList<AbstractMasterServingSize> abstractMaster = new PagedList<AbstractMasterServingSize>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            // param.Add("@Name", AbstractMasterStates.Name, dbType: DbType.String, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServingSize_All, param, commandType: CommandType.StoredProcedure);
                abstractMaster.Values.AddRange(task.Read<MasterServingSize>());
                abstractMaster.TotalRecords=task.Read<long>().SingleOrDefault();
            }
            return abstractMaster;
        }

        public override bool MasterServingSize_Delete(int Id)
        {
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServingSize_Delete, param, commandType: CommandType.StoredProcedure);

            }

            return true;
        }

        public override SuccessResult<AbstractMasterServingSize> MasterServingSize_Id(int Id)
        {
            SuccessResult<AbstractMasterServingSize> MasterTiers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServingSize_Id, param, commandType: CommandType.StoredProcedure);
                MasterTiers=task.Read<SuccessResult<AbstractMasterServingSize>>().SingleOrDefault();
                MasterTiers.Item=task.Read<MasterServingSize>().SingleOrDefault();
            }

            return MasterTiers;
        }

        public override SuccessResult<AbstractMasterServingSize> MasterServingSize_Upsert(AbstractMasterServingSize abstractMasterServingSize)
        {
            SuccessResult<AbstractMasterServingSize> abstractMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterServingSize.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractMasterServingSize.Name, dbType: DbType.String, direction: ParameterDirection.Input);

            using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterServingSize_Upsert, param, commandType: CommandType.StoredProcedure);
                abstractMaster=task.Read<SuccessResult<AbstractMasterServingSize>>().SingleOrDefault();
                abstractMaster.Item=task.Read<MasterServingSize>().SingleOrDefault();
            }

            return abstractMaster;
        }
    }
}
