﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using GlodCleaning.Entities.Contract;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;

namespace TodoDashboard.Data.Contract
{
    public abstract class AbstractMasterSuppliersDao
    {
        public abstract SuccessResult<AbstractMasterSuppliers> MasterSuppliers_Upsert(AbstractMasterSuppliers AbstractMasterSuppliers);

        public abstract SuccessResult<AbstractMasterSuppliers> MasterSuppliers_Id(int Id);

        public abstract PagedList<AbstractMasterSuppliers> MasterSuppliers_All(PageParam pageParam, string search, AbstractMasterSuppliers AbstractMasterSuppliers = null);

        public abstract SuccessResult<AbstractMasterSuppliers> MasterSuppliers_Delete(int Id);
    }
}
