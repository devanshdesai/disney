﻿using Dapper;
using TodoDashboard.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Data.Contract;
using TodoDashboard.Entities.V1;

namespace TodoDashboard.Data.V1
{
    public class PricingTierDao : AbstractPricingTierDao
    {
        public override SuccessResult<AbstractPricingTier> PricingTier_Upsert(AbstractPricingTier AbstractPricingTier)
        {
            SuccessResult<AbstractPricingTier> PricingTier = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractPricingTier.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterWineId", AbstractPricingTier.MasterWineId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterWineVaritalId", AbstractPricingTier.MasterWineVaritalId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterDistributorId", AbstractPricingTier.MasterDistributorId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterSupplierId", AbstractPricingTier.MasterSupplierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterLevelId", AbstractPricingTier.MasterLevelId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterTierId", AbstractPricingTier.MasterTierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Cal5oz", AbstractPricingTier.Cal5oz, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Cal8oz", AbstractPricingTier.Cal8oz, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Cal6oz", AbstractPricingTier.Cal6oz, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CalBottle", AbstractPricingTier.CalBottle, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Price5oz", AbstractPricingTier.Price5oz, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Price8oz", AbstractPricingTier.Price8oz, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@PriceBottle", AbstractPricingTier.PriceBottle, dbType: DbType.Decimal, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PricingTier_Upsert, param, commandType: CommandType.StoredProcedure);
                PricingTier = task.Read<SuccessResult<AbstractPricingTier>>().SingleOrDefault();
                PricingTier.Item = task.Read<PricingTier>().SingleOrDefault();
            }

            return PricingTier;
        }

        public override SuccessResult<AbstractPricingTier> PricingTier_ById(int Id)
        {
            SuccessResult<AbstractPricingTier> PricingTier = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PricingTier_ById, param, commandType: CommandType.StoredProcedure);
                PricingTier = task.Read<SuccessResult<AbstractPricingTier>>().SingleOrDefault();
                PricingTier.Item = task.Read<PricingTier>().SingleOrDefault();
            }

            return PricingTier;
        }

        public override PagedList<AbstractPricingTier> PricingTier_All(PageParam pageParam, string search, AbstractPricingTier abstractPricingTier = null)
        {
            PagedList<AbstractPricingTier> PricingTier = new PagedList<AbstractPricingTier>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MasterWineId", abstractPricingTier.MasterWineId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterWineVaritalId", abstractPricingTier.MasterWineVaritalId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterDistributorId", abstractPricingTier.MasterDistributorId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterSupplierId", abstractPricingTier.MasterSupplierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterLevelId", abstractPricingTier.MasterLevelId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterTierId", abstractPricingTier.MasterTierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PricingTier_All, param, commandType: CommandType.StoredProcedure);
                PricingTier.Values.AddRange(task.Read<PricingTier>());
                PricingTier.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PricingTier;
        }

        public override SuccessResult<AbstractPricingTier> PricingTier_Delete(int Id)
        {
            SuccessResult<AbstractPricingTier> PricingTier = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PricingTier_Delete, param, commandType: CommandType.StoredProcedure);
                PricingTier = task.Read<SuccessResult<AbstractPricingTier>>().SingleOrDefault();
                // PricingTier.Item = task.Read<PricingTier>().SingleOrDefault();
            }

            return PricingTier;
        }
    }
}
