﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;
using TodoDashboard.Infrastructure;
using TodoDashboard.Pages;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Controllers
{
    public class PricingTierController : BaseController
    {
        #region Fields
        public readonly AbstractMasterLocationsService abstractMasterLocationsService;
        public readonly AbstractMasterWinesService abstractMasterWinesService;
        private readonly AbstractMasterWineVaritalsService abstractMasterWineVaritalsService;
        private readonly AbstractMasterBarTypesService abstractMasterBarTypesService;
        private readonly AbstractMasterServicesTypesService abstractMasterServicesTypesService;
        private readonly AbstractMasterTiersService abstractMasterTiersService;
        private readonly AbstractMasterDistributorsService abstractMasterDistributorsService;
        private readonly AbstractMasterStatesService abstractMasterStatesService;
        private readonly AbstractMasterServingSizeServices abstractMasterServingSizeServices;
        private readonly AbstractPricingTierServices abstractPricingTierServices;
        private readonly AbstractMasterLevelServices abstractMasterLevelServices;
        private readonly AbstractMasterSuppliersServices abstractMasterSuppliersServices;
        #endregion

        #region Ctor
        public PricingTierController(AbstractMasterLocationsService abstractMasterLocationsService, AbstractMasterWinesService abstractMasterWinesService,
            AbstractMasterWineVaritalsService abstractMasterWineVaritalsService, AbstractMasterBarTypesService abstractMasterBarTypesService
            , AbstractMasterServicesTypesService abstractMasterServicesTypesService, AbstractMasterTiersService abstractMasterTiersService,
            AbstractMasterDistributorsService abstractMasterDistributorsService, AbstractMasterStatesService abstractMasterStatesService,
            AbstractMasterServingSizeServices abstractMasterServingSizeServices, AbstractPricingTierServices abstractPricingTierServices, AbstractMasterLevelServices abstractMasterLevelServices,
            AbstractMasterSuppliersServices abstractMasterSuppliersServices)
        {
            this.abstractMasterLocationsService = abstractMasterLocationsService;
            this.abstractMasterWinesService = abstractMasterWinesService;
            this.abstractMasterWineVaritalsService = abstractMasterWineVaritalsService;
            this.abstractMasterBarTypesService = abstractMasterBarTypesService;
            this.abstractMasterServicesTypesService = abstractMasterServicesTypesService;
            this.abstractMasterTiersService = abstractMasterTiersService;
            this.abstractMasterDistributorsService = abstractMasterDistributorsService;
            this.abstractMasterStatesService = abstractMasterStatesService;
            this.abstractMasterServingSizeServices = abstractMasterServingSizeServices;
            this.abstractPricingTierServices = abstractPricingTierServices;
            this.abstractMasterSuppliersServices = abstractMasterSuppliersServices;
            this.abstractMasterLevelServices = abstractMasterLevelServices;
        }
        #endregion

        #region Methods
        public ActionResult Index(int Id = 0)
        {
            ViewBag.MasterWines = BindDropDowns("masterwines");
            ViewBag.WineVaritals = BindDropDowns("winevaritals");
            ViewBag.Distributors = BindDropDowns("distributors");
            ViewBag.MasterSuppliers = BindDropDowns("mastersuppliers");
            ViewBag.MasterLevel = BindDropDowns("masterlevel");
            ViewBag.Tiers = BindDropDowns("tiers");
            ViewBag.Id = Id;
            return View();
        }


        [HttpPost]
        public JsonResult Upsert(int Id, int MasterWineId, int MasterWineVaritalId, int MasterDistributorId, int MasterSupplierId, int MasterLevelId, int MasterTierId, decimal Cal5oz, decimal Cal6oz, decimal Cal8oz, decimal CalBottle, decimal Price5oz, decimal Price8oz, decimal PriceBottle)
        {
            AbstractPricingTier abstractPricingTier = new PricingTier();
            abstractPricingTier.Id = Id;
            abstractPricingTier.MasterWineId = MasterWineId;
            abstractPricingTier.MasterWineVaritalId = MasterWineVaritalId;
            abstractPricingTier.MasterDistributorId = MasterDistributorId;
            abstractPricingTier.MasterSupplierId = MasterSupplierId;
            abstractPricingTier.MasterLevelId = MasterLevelId;
            abstractPricingTier.MasterTierId = MasterTierId;
            abstractPricingTier.Cal5oz = Cal5oz;
            abstractPricingTier.Cal6oz = Cal6oz;
            abstractPricingTier.Cal8oz = Cal8oz;
            abstractPricingTier.CalBottle = CalBottle;
            abstractPricingTier.Price5oz = Price5oz;
            abstractPricingTier.Price8oz = Price8oz;
            abstractPricingTier.PriceBottle = PriceBottle;
            SuccessResult<AbstractPricingTier> successResult = abstractPricingTierServices.PricingTier_Upsert(abstractPricingTier);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }



        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                if (action == "winevaritals")
                {
                    var model = abstractMasterWineVaritalsService.MasterWineVaritals_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "tiers")
                {
                    var model = abstractMasterTiersService.MasterTiers_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "distributors")
                {
                    var model = abstractMasterDistributorsService.MasterDistributors_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterwines")
                {
                    AbstractMasterWines abstractMasterWines = new MasterWines();
                    var model = abstractMasterWinesService.MasterWines_All(pageParam, "", abstractMasterWines);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.WineName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterlevel")
                {
                    AbstractMasterLevel abstractMasterLevel = new MasterLevel();
                    var model = abstractMasterLevelServices.MasterLevel_All(pageParam, "", abstractMasterLevel);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "mastersuppliers")
                {
                    AbstractMasterSuppliers abstractMasterSuppliers = new MasterSuppliers();
                    var model = abstractMasterSuppliersServices.MasterSuppliers_All(pageParam, "", abstractMasterSuppliers);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractPricingTier> successResult = abstractPricingTierServices.PricingTier_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);

            #endregion
        }
    }
  
}