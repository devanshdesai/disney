﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Mvc;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;
using TodoDashboard.Infrastructure;
using TodoDashboard.Pages;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Controllers
{
    public class UsersController : BaseController
    {
        #region Fields
        public readonly AbstractMasterUserService abstractMasterUserService;
        #endregion

        #region Ctor
        public UsersController(AbstractMasterUserService abstractMasterUserService)
        {
            this.abstractMasterUserService = abstractMasterUserService;
        }
        #endregion

        #region Methods
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractMasterUserService.MasterUser_All(pageParam, search, null);
                
                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        
        public JsonResult GetUsersId(string Search = "")
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;
                

                var model = abstractMasterUserService.MasterUser_All(pageParam, Search, null);

                totalRecord=(int)model.TotalRecords;
                filteredRecord=(int)model.TotalRecords;

                string ids = "";
                for(int i = 0; i<model.Values.Count; i++)
                {
                    if(i==0)
                    {
                        ids=model.Values[i].Id.ToString();
                    }
                    else
                    {
                        ids=ids+","+model.Values[i].Id.ToString();
                    }
                }

                return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Upsert(int Id,string Name,string Email, string Password)
        {
            AbstractMasterUser abstractMasterUser = new MasterUser();
            abstractMasterUser.Id = Id;
            abstractMasterUser.Name = Name;
            abstractMasterUser.Email = Email;
            abstractMasterUser.Password = Password;
            SuccessResult<AbstractMasterUser> successResult = abstractMasterUserService.MasterUser_Upsert(abstractMasterUser);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ExportExcel(string ids = "")
        {
            

            string var_sql = $"select Name,Email,Password from MasterUser Where UserType = 2";
            
            if(!string.IsNullOrEmpty(ids))
            {
                var_sql+="  And Id in ({ids})";
            }

            try
            {
                ExcelPackage.LicenseContext=LicenseContext.NonCommercial;
                using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter(var_sql, con);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);
                    using(var excelPackage = new ExcelPackage())
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.Add("sheet 1");
                        worksheet.Cells["A1"].LoadFromDataTable(dt, true, TableStyles.None);
                        Session["DownloadExcel_FileManager"]=excelPackage.GetAsByteArray();
                    }
                }

                return Json(new {Code=200,Message="File Created Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { Code = 400, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Delete(string ids)
        {
            
            
            try
            {
                if(string.IsNullOrEmpty(ids))
                {
                    throw new Exception("");
                }
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                string var_sql = "delete from MasterUser where Id in ("+ids+")";
                con.Open();
                SqlCommand cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractMasterUser> successResult = abstractMasterUserService.MasterUser_Id(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}