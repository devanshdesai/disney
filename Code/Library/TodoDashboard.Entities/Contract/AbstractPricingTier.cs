﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoDashboard.Entities.Contract
{
    public abstract class AbstractPricingTier
    {
        public int Id { get; set; }
        public int MasterWineId { get; set; }
        public int MasterWineVaritalId { get; set; }
        public int MasterDistributorId { get; set; }
        public int MasterSupplierId { get; set; }
        public int MasterLevelId { get; set; }
        public int MasterTierId { get; set; }
        public decimal Cal5oz { get; set; }
        public decimal Cal8oz { get; set; }
        public decimal Cal6oz { get; set; }
        public decimal CalBottle { get; set; }
        public decimal Price5oz { get; set; }
        public decimal Price8oz { get; set; }
        public decimal PriceBottle { get; set; }
        public string WineName { get; set; }
        public string WineVaritalName { get; set; }
        public string DistributorName { get; set; }
        public string SupplierName { get; set; }
        public string LevelName { get; set; }
        public string TierName { get; set; }
        //[NotMapped]
        //public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("mm/dd/yyyy") : "-";
        //[NotMapped]
        //public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("mm/dd/yyyy") : "-";

    }
}
