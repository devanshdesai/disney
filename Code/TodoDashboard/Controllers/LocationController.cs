﻿using Autofac;
using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;
using TodoDashboard.Infrastructure;
using TodoDashboard.Pages;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Controllers
{
    public class LocationController : BaseController
    {
        #region Fields
        public readonly AbstractMasterLocationsService abstractMasterLocationsService;
        public readonly AbstractMasterWinesService abstractMasterWinesService;
        private readonly AbstractMasterWineVaritalsService abstractMasterWineVaritalsService;
        private readonly AbstractMasterBarTypesService abstractMasterBarTypesService;
        private readonly AbstractMasterServicesTypesService abstractMasterServicesTypesService;
        private readonly AbstractMasterTiersService abstractMasterTiersService;
        private readonly AbstractMasterDistributorsService abstractMasterDistributorsService;
        private readonly AbstractMasterStatesService abstractMasterStatesService;
        private readonly AbstractMasterServingSizeServices abstractMasterServingSizeServices;
        #endregion

        #region Ctor
        public LocationController(AbstractMasterLocationsService abstractMasterLocationsService, AbstractMasterWinesService abstractMasterWinesService,
            AbstractMasterWineVaritalsService abstractMasterWineVaritalsService, AbstractMasterBarTypesService abstractMasterBarTypesService
            , AbstractMasterServicesTypesService abstractMasterServicesTypesService, AbstractMasterTiersService abstractMasterTiersService,
            AbstractMasterDistributorsService abstractMasterDistributorsService, AbstractMasterStatesService abstractMasterStatesService, AbstractMasterServingSizeServices abstractMasterServingSizeServices)
        {
            this.abstractMasterLocationsService=abstractMasterLocationsService;
            this.abstractMasterWinesService=abstractMasterWinesService;
            this.abstractMasterWineVaritalsService=abstractMasterWineVaritalsService;
            this.abstractMasterBarTypesService=abstractMasterBarTypesService;
            this.abstractMasterServicesTypesService=abstractMasterServicesTypesService;
            this.abstractMasterTiersService=abstractMasterTiersService;
            this.abstractMasterDistributorsService=abstractMasterDistributorsService;
            this.abstractMasterStatesService=abstractMasterStatesService;
            this.abstractMasterServingSizeServices=abstractMasterServingSizeServices;
        }
        #endregion

        #region Methods
        public ActionResult Index(int Id = 0)
        {
            ViewBag.States=BindDropDowns("states");
            ViewBag.ServiceTypes=BindDropDowns("servicetypes");
            ViewBag.BarTypes=BindDropDowns("bartypes");
            ViewBag.ServingSizeId=BindDropDowns("MasterServingSize");
            //ViewBag.WineVaritals = BindDropDowns("winevaritals");
            //ViewBag.Distributors = BindDropDowns("distributors");
            //ViewBag.Tiers = BindDropDowns("tiers");
            ViewBag.Id=Id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindWineData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int LocationId = 0, bool IsAdd = false)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;
                int count = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=requestModel.Start;
                pageParam.Limit=requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractMasterWines abstractMasterWines = new MasterWines();

                var model = abstractMasterWinesService.MasterWines_All(pageParam, search, abstractMasterWines, LocationId, IsAdd);
                //List<LocationWines> selectedWines = new List<LocationWines>();
                //selectedWines = GetCheckedWines(LocationId);

                //if(selectedWines.Count > 0 && LocationId > 0)
                //{
                //    model.Values = model.Values
                //    foreach (var data in model.Values)
                //    {
                //        //LocationWines locationWines = new LocationWines();
                //        //locationWines.MasterWineId = data.Id;
                //        if (selectedWines.Find(x => x.MasterWineId == data.Id) != null && selectedWines.Find(x => x.MasterWineId == data.Id).MasterWineId != 0)
                //        {
                //            //data.IsChecked = true;
                //        }
                //        else
                //        {
                //            //data.IsChecked = false;
                //            model.Values.RemoveAt(count);
                //        }

                //        count++;
                //    }
                //    model.TotalRecords = model.Values.Count;                    
                //}

                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetWineId(string Search="",int LocationId = 0, bool IsAdd = false)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;
                

                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;

                

                AbstractMasterWines abstractMasterWines = new MasterWines();

                var model = abstractMasterWinesService.MasterWines_All(pageParam, Search, abstractMasterWines, LocationId, IsAdd);
                

                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;

                string ids = "";
                for(int i = 0; i<model.Values.Count; i++)
                {
                    if(i==0)
                    {
                        ids=model.Values[i].Id.ToString();
                    }
                    else
                    {
                        ids=ids+","+model.Values[i].Id.ToString();
                    }
                }

                return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Upsert(int Id, string LocationId, string LocationName, int StateId, int ServiceTypeId, int BarTypeId, string WineIds = "")
        {
            string[] WineIdsArr = WineIds.Split(',');

            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlDataAdapter ad = null;
            string query = "";
            DataTable dt = new DataTable();
            if(WineIdsArr[0]=="All"&&WineIdsArr.Length>1)
            {
                WineIds=WineIds.Replace("All,", "");
                query=$"select * from MasterWines where id not in ({WineIds})";
            }
            else if(WineIdsArr[0]=="All")
            {
                query="select * from MasterWines";
            }
            else
            {
                query=$"select * from MasterWines where id  in ({WineIds})";
            }

            try
            {
                ad=new SqlDataAdapter(query, con);
                ad.Fill(dt);
            }
            catch
            {

            }


            for(int i = 0; i<dt.Rows.Count; i++)
            {
                if(i==0)
                {
                    WineIds=dt.Rows[i]["Id"].ToString();
                }
                //else if(i==dt.Rows.Count-1)
                //{
                //    WineIds+=dt.Rows[0]["Id"].ToString();
                //}
                else
                {
                    WineIds=WineIds+","+dt.Rows[i]["Id"].ToString();
                }
            }

            AbstractMasterLocations abstractMasterLocations = new MasterLocations();
            abstractMasterLocations.Id=Id;
            abstractMasterLocations.LocationId=LocationId;
            abstractMasterLocations.LocationName=LocationName;
            abstractMasterLocations.StateId=StateId;
            abstractMasterLocations.ServiceTypeId=ServiceTypeId;
            abstractMasterLocations.BarTypeId=BarTypeId;

            SuccessResult<AbstractMasterLocations> result = abstractMasterLocationsService.MasterLocations_Upsert(abstractMasterLocations);


            try
            {
                if(Id==0&&result!=null&&result.Code==200&&result.Item!=null&&WineIds!=null&&WineIds!="")
                {
                    // ** Abbas -> WineIds=WineIds.Substring(0, WineIds.Length-1);
                    InsertLocationWines(WineIds, result.Item.Id);
                }
                else if(Id==0&&result!=null&&result.Code==200&&result.Item!=null)
                {
                    InsertLocationWines(WineIds, result.Item.Id);
                }
            }
            catch(Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpsertWine(string WineIds, int Id)
        {
            string[] WineIdsArr = WineIds.Split(',');

            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlDataAdapter ad = null;
            string query = "";
            DataTable dt = new DataTable();

            if(WineIdsArr[0]=="All"&&WineIdsArr.Length>1)
            {
                WineIds=WineIds.Replace("All,", "");
                query=$"select * from MasterWines where id not in ({WineIds})";
            }
            else if(WineIdsArr[0]=="All")
            {
                query="select * from MasterWines";
            }
            else
            {
                query=$"select * from MasterWines where id  in ({WineIds})";
            }

            try
            {
                ad=new SqlDataAdapter(query, con);
                ad.Fill(dt);
            }
            catch
            {

            }


            for(int i = 0; i<dt.Rows.Count; i++)
            {
                if(i==0)
                {
                    WineIds=dt.Rows[i]["Id"].ToString();
                }
                //else if(i==dt.Rows.Count-1)
                //{
                //    WineIds+=dt.Rows[0]["Id"].ToString();
                //}
                else
                {
                    WineIds=WineIds+","+dt.Rows[i]["Id"].ToString();
                }
            }
            IsAddInsertLocationWines(WineIds, Id);
            return Json("200", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ExportExcel(string ids = "", int id = 0)
        {
            ExcelPackage.LicenseContext=LicenseContext.NonCommercial;

            using(var excelPackage = new ExcelPackage())
            {
                var worksheet = excelPackage.Workbook.Worksheets.Add("sheet 1");

                using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    string Query = $"select LocationId AS 'Id',LocationName  AS 'Location',MS.Name AS 'State',St.Name as 'Service Type',BT.Name AS 'Bar Type' from MasterLocations ML LEFT JOIN MasterStates MS ON ML.StateId =  MS.Id LEFT JOIN MasterServicesTypes ST ON ML.ServiceTypeId = ST.Id LEFT JOIN MasterBarTypes BT ON ML.BarTypeId = BT.Id  WHERE ML.Id  = {id}";


                    DataTable dtExcel = new DataTable();


                    DataTable dtLocation = new DataTable();
                    SqlDataAdapter adLocation = new SqlDataAdapter(Query, con);
                    adLocation.Fill(dtLocation);

                    Query=$"select  WineName AS 'Wine Name',MV.Name AS 'Varietals Name',Calories AS 'Calories',SS.Name AS 'Serving Size',ISNULL(MD.Name,'') AS 'Distributor',ISNULL(MT.Name,'') AS 'Tier'  from LocationWines LW LEFT JOIN MasterWines MW ON LW.MasterWineId = MW.Id LEFT JOIN MasterServingSize SS ON LW.MasterServingSizeId = SS.Id LEFT JOIN MasterDistributors MD ON MW.MasterDistributorId = MD.Id LEFT JOIN MasterTiers MT ON MW.TierId = MT.Id LEFT JOIN MasterWineVaritals MV ON MW.MasterVarietalId = MV.Id  WHERE LW.MasterLocationId = {id}";
                    if(!string.IsNullOrEmpty(ids))
                    {
                        Query=Query+$"AND LW.MasterWineId in ({ ids})";
                    }
                    DataTable dtWine = new DataTable();
                    SqlDataAdapter adWine = new SqlDataAdapter(Query, con);
                    adWine.Fill(dtWine);

                    foreach(DataColumn item in dtLocation.Columns)
                    {
                        dtExcel.Columns.Add(item.ColumnName.ToString());
                    }



                    foreach(DataColumn item in dtWine.Columns)
                    {
                        DataColumnCollection columns = dtExcel.Columns;
                        if(!dtExcel.Columns.Contains(item.ColumnName.ToString()))
                            dtExcel.Columns.Add(item.ColumnName.ToString());
                    }
                    
                    for(int j = 0; j<dtWine.Rows.Count; j++)
                    {
                        DataRow row = dtExcel.NewRow();
                        if(j==0)
                        {
                            row["Id"]=dtLocation.Rows[0]["Id"].ToString();
                            row["Location"]=dtLocation.Rows[0]["Location"].ToString();
                            row["State"]=dtLocation.Rows[0]["State"].ToString();
                            row["Service Type"]=dtLocation.Rows[0]["Service Type"].ToString();
                            row["Bar Type"]=dtLocation.Rows[0]["Bar Type"].ToString();
                        }

                    
                        row["Wine Name"]=dtWine.Rows[j]["Wine Name"].ToString();
                        row["Varietals Name"]=dtWine.Rows[j]["Varietals Name"].ToString();
                        row["Calories"]=dtWine.Rows[j]["Calories"].ToString();
                        row["Serving Size"]=dtWine.Rows[j]["Serving Size"].ToString();
                        row["Distributor"]=dtWine.Rows[j]["Distributor"].ToString();
                        row["Tier"]=dtWine.Rows[j]["Tier"].ToString();
                        dtExcel.Rows.Add(row);

                    }



                    worksheet.Cells["A1"].LoadFromDataTable(dtExcel, true, TableStyles.None);

                    Session["DownloadExcel_FileManager"]=excelPackage.GetAsByteArray();
                }
            }
            return Json(new { Code = 200, Message = "File Created Successfully" });
        }
        [HttpPost]
        public JsonResult DeleteSelectedWinesFromLocation(int LocationId, string WineIds = "")
        {
            if(LocationId > 0 && WineIds != string.Empty && WineIds!=null && WineIds!="")
            {
                // Abbaas -> ** WineIds=WineIds.Substring(0, WineIds.Length-1);
                DeleteSelectedWines(LocationId, WineIds);
                return Json("200", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("400", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractMasterLocations> successResult = abstractMasterLocationsService.MasterLocations_Id(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }


        public List<LocationWines> GetCheckedWines(int LocationId)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql = "select MasterWineId from LocationWines where MasterLocationId="+LocationId;
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            sda.Fill(dt);
            return CommonHelper.ConvertDataTable<LocationWines>(dt);
        }

        public void InsertLocationWines(string WineIds, int LocationId)
        {
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql = "delete from LocationWines where MasterLocationId="+LocationId;
            SqlCommand cmd = new SqlCommand(var_sql, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            if(WineIds!="")
            {
                var_sql="insert into LocationWines (MasterLocationId,MasterWineId,MasterServingSizeId) values ";
                string[] wineId = WineIds.Split(',');
                for(int start = 0; start<wineId.Length; start++)
                {
                    if(start==0)
                    {
                        var_sql+="("+LocationId+","+wineId[start]+",(select top 1 Id from MasterServingSize Order By id asc)"+")";
                    }
                    else
                    {
                        var_sql+=",("+LocationId+","+wineId[start]+",(select top 1 Id from MasterServingSize Order By id asc)"+")";
                    }
                }

                cmd=new SqlCommand(var_sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        public void IsAddInsertLocationWines(string WineIds, int LocationId)
        {
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlCommand cmd = null;
            string var_sql = "";
            if(WineIds!="")
            {
                var_sql="insert into LocationWines (MasterLocationId,MasterWineId,MasterServingSizeId) values ";
                string[] wineId = WineIds.Split(',');
                for(int start = 0; start<wineId.Length; start++)
                {
                    if(start==0)
                    {
                        var_sql+="("+LocationId+","+wineId[start]+",(select top 1 Id from MasterServingSize Order By id asc)"+")";
                    }
                    else
                    {
                        var_sql+=",("+LocationId+","+wineId[start]+",(select top 1 Id from MasterServingSize Order By id asc)"+")";
                    }
                }

                cmd=new SqlCommand(var_sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public JsonResult UpdateServingSizeId(int WineId, int LocationId, int MasterServingSizeId)
        {
            try
            {

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                SqlCommand cmd = null;
                string var_sql = "";

                var_sql="Update LocationWines SET MasterServingSizeId = "+MasterServingSizeId+" where MasterLocationId="+LocationId+" and MasterWineId =  "+WineId;

                cmd=new SqlCommand(var_sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return Json("200", JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json("400", JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;

                if(action=="winevaritals")
                {
                    var model = abstractMasterWineVaritalsService.MasterWineVaritals_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="bartypes")
                {
                    var model = abstractMasterBarTypesService.MasterBarTypes_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="servicetypes")
                {
                    var model = abstractMasterServicesTypesService.MasterServicesTypes_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="tiers")
                {
                    var model = abstractMasterTiersService.MasterTiers_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="distributors")
                {
                    var model = abstractMasterDistributorsService.MasterDistributors_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="states")
                {
                    var model = abstractMasterStatesService.MasterStates_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="MasterServingSize")
                {
                    var model = abstractMasterServingSizeServices.MasterServingSize_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                return items;
            }
            catch(Exception ex)
            {
                return items;
            }
        }

        public void DeleteSelectedWines(int LocationId, string WineIds = "")
        {
            string var_sql = "delete from LocationWines where MasterLocationId="+LocationId+" and MasterWineId in ("+WineIds+")";
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand(var_sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public JsonResult Delete(int ids)
        {
            string var_sql = "";
            var_sql="delete from MasterLocations where Id="+ids;

            try
            {
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
                var_sql="delete from LocationWines where MasterLocationId="+ids;
                con.Open();
                cmd=new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        public class LocationWines
        {
            public int MasterWineId { get; set; }
        }
    }
}