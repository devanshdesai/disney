﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;

namespace TodoDashboard.Services.Contract
{
    public abstract class AbstractPricingTierServices
    {
        public abstract SuccessResult<AbstractPricingTier> PricingTier_Upsert(AbstractPricingTier AbstractPricingTier);

        public abstract SuccessResult<AbstractPricingTier> PricingTier_ById(int Id);

        public abstract PagedList<AbstractPricingTier> PricingTier_All(PageParam pageParam, string search, AbstractPricingTier AbstractPricingTier = null);

        public abstract SuccessResult<AbstractPricingTier> PricingTier_Delete(int Id);
    }
}
