﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Data.Contract;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Services.V1
{
    public class MasterSuppliersServices : AbstractMasterSuppliersServices
    {
        private AbstractMasterSuppliersDao abstractMasterSuppliersDao;

        public MasterSuppliersServices(AbstractMasterSuppliersDao abstractMasterSuppliersDao)
        {
            this.abstractMasterSuppliersDao = abstractMasterSuppliersDao;
        }

        public override SuccessResult<AbstractMasterSuppliers> MasterSuppliers_Upsert(AbstractMasterSuppliers AbstractMasterSuppliers)
        {
            return abstractMasterSuppliersDao.MasterSuppliers_Upsert(AbstractMasterSuppliers);
        }

        public override SuccessResult<AbstractMasterSuppliers> MasterSuppliers_Id(int Id)
        {
            return abstractMasterSuppliersDao.MasterSuppliers_Id(Id);
        }

        public override PagedList<AbstractMasterSuppliers> MasterSuppliers_All(PageParam pageParam, string search, AbstractMasterSuppliers AbstractMasterSuppliers = null)
        {
            return abstractMasterSuppliersDao.MasterSuppliers_All(pageParam, search, AbstractMasterSuppliers);
        }

        public override SuccessResult<AbstractMasterSuppliers> MasterSuppliers_Delete(int Id)
        {
            return this.abstractMasterSuppliersDao.MasterSuppliers_Delete(Id);
        }
    }
}
