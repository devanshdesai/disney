﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;
using TodoDashboard.Infrastructure;
using TodoDashboard.Pages;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Controllers
{
    public class WineController : BaseController
    {
        #region Fields
        public readonly AbstractMasterLocationsService abstractMasterLocationsService;
        public readonly AbstractMasterWinesService abstractMasterWinesService;
        private readonly AbstractMasterWineVaritalsService abstractMasterWineVaritalsService;
        private readonly AbstractMasterBarTypesService abstractMasterBarTypesService;
        private readonly AbstractMasterServicesTypesService abstractMasterServicesTypesService;
        private readonly AbstractMasterTiersService abstractMasterTiersService;
        private readonly AbstractMasterDistributorsService abstractMasterDistributorsService;
        private readonly AbstractMasterStatesService abstractMasterStatesService;
        private readonly AbstractMasterServingSizeServices abstractMasterServingSizeServices;
        #endregion

        #region Ctor
        public WineController(AbstractMasterLocationsService abstractMasterLocationsService, AbstractMasterWinesService abstractMasterWinesService,
            AbstractMasterWineVaritalsService abstractMasterWineVaritalsService, AbstractMasterBarTypesService abstractMasterBarTypesService
            , AbstractMasterServicesTypesService abstractMasterServicesTypesService, AbstractMasterTiersService abstractMasterTiersService,
            AbstractMasterDistributorsService abstractMasterDistributorsService, AbstractMasterStatesService abstractMasterStatesService,
            AbstractMasterServingSizeServices abstractMasterServingSizeServices)
        {
            this.abstractMasterLocationsService = abstractMasterLocationsService;
            this.abstractMasterWinesService = abstractMasterWinesService;
            this.abstractMasterWineVaritalsService = abstractMasterWineVaritalsService;
            this.abstractMasterBarTypesService = abstractMasterBarTypesService;
            this.abstractMasterServicesTypesService = abstractMasterServicesTypesService;
            this.abstractMasterTiersService = abstractMasterTiersService;
            this.abstractMasterDistributorsService = abstractMasterDistributorsService;
            this.abstractMasterStatesService = abstractMasterStatesService;
            this.abstractMasterServingSizeServices=abstractMasterServingSizeServices;
        }
        #endregion

        #region Methods
        public ActionResult Index(int Id = 0)
        {
            ViewBag.WineVaritals = BindDropDowns("winevaritals");
            ViewBag.Distributors = BindDropDowns("distributors");
            ViewBag.Tiers = BindDropDowns("tiers");
            ViewBag.ServingSizeId=BindDropDowns("MasterServingSize");
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindLocationData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int WineId = 0,bool IsAdd= false)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                

                var model = abstractMasterLocationsService.MasterLocations_All(pageParam, search, abstractMasterLocations,WineId,IsAdd);
                //List<LocationWines> selectedWines = new List<LocationWines>();
                //selectedWines = GetCheckedWines(WineId);

                //if (selectedWines.Count > 0)
                //{
                //    foreach (var data in model.Values)
                //    {
                //        LocationWines locationWines = new LocationWines();
                //        locationWines.MasterLocationId = data.Id;
                //        if (selectedWines.Find(x => x.MasterLocationId == data.Id) != null && selectedWines.Find(x => x.MasterLocationId == data.Id).MasterLocationId != 0)
                //        {
                //            data.IsChecked = true;
                //        }
                //        else
                //        {
                //            data.IsChecked = false;
                //        }
                //    }
                //}


                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;


                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetLocationId(string Search = "", int WineId = 0, bool IsAdd = false)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;


                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;



                AbstractMasterLocations abstractMasterLocations = new MasterLocations();


                var model = abstractMasterLocationsService.MasterLocations_All(pageParam, Search, abstractMasterLocations, WineId, IsAdd);


                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;

                string ids = "";
                for(int i = 0; i<model.Values.Count; i++)
                {
                    if(i==0)
                    {
                        ids=model.Values[i].Id.ToString();
                    }
                    else
                    {
                        ids=ids+","+model.Values[i].Id.ToString();
                    }
                }

                return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Upsert(int Id, string WineName,int MasterVarietalId, string Calories, int MasterDistributorId, int TierId,string LocationIds="")
        {
            string[] LocationIdsArr = LocationIds.Split(',');

            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlDataAdapter ad = null;
            string query = "";
            DataTable dt = new DataTable();
            
            if(LocationIdsArr[0]=="All"&&LocationIdsArr.Length>1)
            {
                LocationIds = LocationIds.Replace("All,", "");
                query =$"select * from MasterLocations where id not in ({LocationIds})";
            }
            else if (LocationIdsArr[0] == "All")
            {
                query = "select * from MasterLocations";
            }
            else
            {
                query=$"select * from MasterLocations where id  in ({LocationIds})";
            }

            try
            {
                ad = new SqlDataAdapter(query, con);
                ad.Fill(dt);
            }
            catch
            {

            }
            
            for(int i = 0; i<dt.Rows.Count; i++)
            {
                if(i==0)
                {
                    LocationIds=dt.Rows[i]["Id"].ToString();
                }
                //else if(i==dt.Rows.Count-1)
                //{
                //    WineIds+=dt.Rows[0]["Id"].ToString();
                //}
                else
                {
                    LocationIds=LocationIds+","+dt.Rows[i]["Id"].ToString();
                }
            }

            AbstractMasterWines abstractMasterWines = new MasterWines();
            abstractMasterWines.Id = Id;
            abstractMasterWines.WineName = WineName;
            abstractMasterWines.MasterVarietalId = MasterVarietalId;
            abstractMasterWines.Calories = Calories;
            abstractMasterWines.MasterDistributorId = MasterDistributorId;
            abstractMasterWines.TierId = TierId;

            SuccessResult<AbstractMasterWines> result = abstractMasterWinesService.MasterWines_Upsert(abstractMasterWines);

            try
            {
                if (Id == 0 && result != null && result.Code == 200 && result.Item != null && LocationIds != null && LocationIds != "")
                {
                    InsertLocationWines(LocationIds, result.Item.Id);
                }
                else if (Id == 0 && result != null && result.Code == 200 && result.Item != null)
                {
                    InsertLocationWines(LocationIds, result.Item.Id);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpsertLocation(string WineIds, int Id)
        {
            string[] LocationIdsArr = WineIds.Split(',');

            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlDataAdapter ad = null;
            string query = "";
            DataTable dt = new DataTable();


            if(LocationIdsArr[0]=="All"&&LocationIdsArr.Length>1)
            {
                WineIds = WineIds.Replace("All,", "");
                query =$"select * from MasterLocations where id not in ({WineIds})";
            }
            else if (LocationIdsArr[0] == "All")
            {
                query = "select * from MasterLocations";
            }
            else
            {
                query=$"select * from MasterLocations where id  in ({WineIds})";
            }

            try
            {
                ad = new SqlDataAdapter(query, con);
                ad.Fill(dt);
            }
            catch
            {

            }
           

            for(int i = 0; i<dt.Rows.Count; i++)
            {
                if(i==0)
                {
                    WineIds=dt.Rows[i]["Id"].ToString();
                }
                //else if(i==dt.Rows.Count-1)
                //{
                //    WineIds+=dt.Rows[0]["Id"].ToString();
                //}
                else
                {
                    WineIds=WineIds+","+dt.Rows[i]["Id"].ToString();
                }
            }
            // WineIds Is Actually Location Ids
            IsAddInsertLocationWines(WineIds,Id);
            return Json("200", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSelectedLocationsFromWine(int WineId, string LocationIds = "")
        {
            if (WineId > 0 && LocationIds != string.Empty && LocationIds != null && LocationIds != "")
            {
                
                DeleteSelectedLocations(WineId, LocationIds);
                return Json("200", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("400", JsonRequestBehavior.AllowGet);
            }
           
        }

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractMasterWines> successResult = abstractMasterWinesService.MasterWines_Id(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ExportExcel(string ids = "", int id = 0)
        {
            
            ExcelPackage.LicenseContext=LicenseContext.NonCommercial;

            using(var excelPackage = new ExcelPackage())
            {
                var worksheet = excelPackage.Workbook.Worksheets.Add("sheet 1");

                using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    string Query = $"select WineName AS 'Wine Name',MV.Name AS 'Varietals Name',Calories AS 'Calories',ISNULL(MD.Name,'') AS 'Distributor',ISNULL(MT.Name,'') AS 'Tier' from MasterWines MW LEFT JOIN MasterDistributors MD ON MW.MasterDistributorId = MD.Id LEFT JOIN MasterTiers MT ON MW.TierId = MT.Id LEFT JOIN MasterWineVaritals MV ON MW.MasterVarietalId = MV.Id WHERE MW.Id = ({id})";


                    DataTable dtExcel = new DataTable();


                    DataTable dtWine = new DataTable();
                    SqlDataAdapter adLocation = new SqlDataAdapter(Query, con);
                    adLocation.Fill(dtWine);

                    Query=$"select ML.LocationId AS 'Id',ML.LocationName AS 'Location',SS.Name AS 'Serving Size',MS.Name AS 'State',ST.Name AS 'Service Type',BT.Name AS 'Bar Type' from LocationWines LW  LEFT JOIN MasterLocations ML ON LW.MasterLocationId = ML.Id  LEFT JOIN MasterStates MS ON MS.Id = ML.StateId LEFT JOIN MasterServicesTypes ST ON ML.ServiceTypeId = ST.Id LEFT JOIN MasterBarTypes BT ON ML.BarTypeId = BT.Id LEFT JOIN MasterServingSize SS ON SS.Id = LW.MasterServingSizeId WHERE LW.MasterWineId = {id} ";

                    if(!string.IsNullOrEmpty(ids))
                    {
                        Query=Query+$" AND LW.MasterLocationId in ({ids})";
                    }
                    DataTable dtLocation = new DataTable();
                    SqlDataAdapter adWine = new SqlDataAdapter(Query, con);
                    adWine.Fill(dtLocation);

                    foreach(DataColumn item in dtWine.Columns)
                    {
                        dtExcel.Columns.Add(item.ColumnName.ToString());
                    }



                    foreach(DataColumn item in dtLocation.Columns)
                    {
                        DataColumnCollection columns = dtExcel.Columns;
                        if(!dtExcel.Columns.Contains(item.ColumnName.ToString()))
                            dtExcel.Columns.Add(item.ColumnName.ToString());
                    }

                    for(int j = 0; j<dtLocation.Rows.Count; j++)
                    {
                        DataRow row = dtExcel.NewRow();
                        if(j==0)
                        {
                            row["Wine Name"]=dtWine.Rows[0]["Wine Name"].ToString();
                            row["Varietals Name"]=dtWine.Rows[0]["Varietals Name"].ToString();
                            row["Calories"]=dtWine.Rows[0]["Calories"].ToString();
                            row["Distributor"]=dtWine.Rows[0]["Distributor"].ToString();
                            row["Tier"]=dtWine.Rows[0]["Tier"].ToString();

                          
                        }

                        row["Id"]=dtLocation.Rows[j]["Id"].ToString();
                        row["Location"]=dtLocation.Rows[j]["Location"].ToString();
                        row["State"]=dtLocation.Rows[j]["State"].ToString();
                        row["Service Type"]=dtLocation.Rows[j]["Service Type"].ToString();
                        row["Bar Type"]=dtLocation.Rows[j]["Bar Type"].ToString();
                        row["Serving Size"]=dtLocation.Rows[j]["Serving Size"].ToString();

                        
                        dtExcel.Rows.Add(row);

                    }



                    worksheet.Cells["A1"].LoadFromDataTable(dtExcel, true, TableStyles.None);

                    Session["DownloadExcel_FileManager"]=excelPackage.GetAsByteArray();
                }
            }
            return Json(new { Code = 200, Message = "File Created Successfully" });
        }
        public List<LocationWines> GetCheckedWines(int WineId)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql = "select MasterLocationId from LocationWines where MasterWineId=" + WineId;
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            sda.Fill(dt);
            return CommonHelper.ConvertDataTable<LocationWines>(dt);
        }

        public void InsertLocationWines(string LocationIds, int WineId)
        {
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql = "delete from LocationWines where MasterWineId=" + WineId;
            SqlCommand cmd = new SqlCommand(var_sql, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            if (LocationIds != "")
            {
                var_sql = "insert into LocationWines (MasterWineId,MasterLocationId,MasterServingSizeId) values ";
                string[] wineId = LocationIds.Split(',');
                for (int start = 0; start < wineId.Length; start++)
                {
                    if (start == 0)
                    {
                        var_sql += "(" + WineId + "," + wineId[start] +",(select top 1 Id from MasterServingSize Order By id asc)"+ ")";
                    }
                    else
                    {
                        var_sql += ",(" + WineId + "," + wineId[start] +",(select top 1 Id from MasterServingSize Order By id asc)"+ ")";
                    }
                }

                cmd = new SqlCommand(var_sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void IsAddInsertLocationWines(string LocationIds, int WineId)
        {
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql ="";
            SqlCommand cmd = null;
            

            if(LocationIds!="")
            {
                var_sql="insert into LocationWines (MasterWineId,MasterLocationId,MasterServingSizeId) values ";
                string[] locationIds = LocationIds.Split(',');
                for(int start = 0; start<locationIds.Length; start++)
                {
                    if(start==0)
                    {
                        var_sql+="("+WineId+","+locationIds[start]+",(select Top 1 Id from MasterServingSize order by Id asc)"+")";
                    }
                    else
                    {
                        var_sql+=",("+WineId+","+locationIds[start]+",(select Top 1 Id from MasterServingSize order by Id asc)"+")";
                    }
                }

                cmd=new SqlCommand(var_sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        public JsonResult UpdateServingSizeId(int WineId, int LocationId, int MasterServingSizeId)
        {
            try
            {
                
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                SqlCommand cmd = null;
                string var_sql = "";

                var_sql="Update LocationWines SET MasterServingSizeId = "+MasterServingSizeId+" where MasterLocationId="+LocationId+" and MasterWineId = "+WineId;

                cmd=new SqlCommand(var_sql, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return Json("200", JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json("400", JsonRequestBehavior.AllowGet);
            }
        }
        public void DeleteSelectedLocations(int WineId, string LocationIds)
        {
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            
            string var_sql = "delete from LocationWines where MasterWineId=" + WineId + " and MasterLocationId in (" + LocationIds + ")";
            
            con.Open();
            SqlCommand cmd = new SqlCommand(var_sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public JsonResult Delete(int ids)
        {
            string var_sql = "";
            var_sql = "delete from MasterWines where Id=" + ids;

            try
            {
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
                var_sql = "delete from LocationWines where MasterWineId=" + ids;
                con.Open();
                cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                if (action == "winevaritals")
                {
                    var model = abstractMasterWineVaritalsService.MasterWineVaritals_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "bartypes")
                {
                    var model = abstractMasterBarTypesService.MasterBarTypes_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "servicetypes")
                {
                    var model = abstractMasterServicesTypesService.MasterServicesTypes_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "tiers")
                {
                    var model = abstractMasterTiersService.MasterTiers_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "distributors")
                {
                    var model = abstractMasterDistributorsService.MasterDistributors_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "states")
                {
                    var model = abstractMasterStatesService.MasterStates_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if(action=="MasterServingSize")
                {
                    var model = abstractMasterServingSizeServices.MasterServingSize_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }
        #endregion
    }

    public class LocationWines
    {
        public int MasterLocationId { get; set; }
    }
}