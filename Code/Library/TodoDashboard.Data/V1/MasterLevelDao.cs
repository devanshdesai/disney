﻿using Dapper;
using TodoDashboard.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Data.Contract;
using TodoDashboard.Entities.V1;

namespace TodoDashboard.Data.V1
{
    public class MasterLevelDao : AbstractMasterLevelDao
    {
        public override SuccessResult<AbstractMasterLevel> MasterLevel_Upsert(AbstractMasterLevel AbstractMasterLevel)
        {
            SuccessResult<AbstractMasterLevel> MasterLevel = null;
            var param = new DynamicParameters();

            param.Add("@Id", AbstractMasterLevel.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", AbstractMasterLevel.Name, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterLevel_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterLevel = task.Read<SuccessResult<AbstractMasterLevel>>().SingleOrDefault();
                MasterLevel.Item = task.Read<MasterLevel>().SingleOrDefault();
            }

            return MasterLevel;
        }

        public override SuccessResult<AbstractMasterLevel> MasterLevel_Id(int Id)
        {
            SuccessResult<AbstractMasterLevel> MasterLevel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterLevel_Id, param, commandType: CommandType.StoredProcedure);
                MasterLevel = task.Read<SuccessResult<AbstractMasterLevel>>().SingleOrDefault();
                MasterLevel.Item = task.Read<MasterLevel>().SingleOrDefault();
            }

            return MasterLevel;
        }

        public override PagedList<AbstractMasterLevel> MasterLevel_All(PageParam pageParam, string search, AbstractMasterLevel AbstractMasterLevel = null)
        {
            PagedList<AbstractMasterLevel> MasterLevel = new PagedList<AbstractMasterLevel>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterLevel_All, param, commandType: CommandType.StoredProcedure);
                MasterLevel.Values.AddRange(task.Read<MasterLevel>());
                MasterLevel.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterLevel;
        }

        public override SuccessResult<AbstractMasterLevel> MasterLevel_Delete(int Id)
        {
            SuccessResult<AbstractMasterLevel> MasterLevel = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterLevel_Delete, param, commandType: CommandType.StoredProcedure);
                MasterLevel = task.Read<SuccessResult<AbstractMasterLevel>>().SingleOrDefault();
                // MasterLevel.Item = task.Read<MasterLevel>().SingleOrDefault();
            }

            return MasterLevel;
        }
    }
}
