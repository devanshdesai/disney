﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Data.Contract;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Services.V1
{
    public class MasterLevelServices : AbstractMasterLevelServices
    {
        private AbstractMasterLevelDao abstractMasterLevelDao;

        public MasterLevelServices(AbstractMasterLevelDao abstractMasterLevelDao)
        {
            this.abstractMasterLevelDao = abstractMasterLevelDao;
        }

        public override SuccessResult<AbstractMasterLevel> MasterLevel_Upsert(AbstractMasterLevel AbstractMasterLevel)
        {
            return abstractMasterLevelDao.MasterLevel_Upsert(AbstractMasterLevel);
        }

        public override SuccessResult<AbstractMasterLevel> MasterLevel_Id(int Id)
        {
            return abstractMasterLevelDao.MasterLevel_Id(Id);
        }

        public override PagedList<AbstractMasterLevel> MasterLevel_All(PageParam pageParam, string search, AbstractMasterLevel AbstractMasterLevel = null)
        {
            return abstractMasterLevelDao.MasterLevel_All(pageParam, search, AbstractMasterLevel);
        }

        public override SuccessResult<AbstractMasterLevel> MasterLevel_Delete(int Id)
        {
            return this.abstractMasterLevelDao.MasterLevel_Delete(Id);
        }
    }
}
