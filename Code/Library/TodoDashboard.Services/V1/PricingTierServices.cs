﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Data.Contract;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Services.Contract;

namespace TodoDashboard.Services.V1
{
    public class PricingTierServices : AbstractPricingTierServices
    {
        private AbstractPricingTierDao abstractPricingTierDao;

        public PricingTierServices(AbstractPricingTierDao abstractPricingTierDao)
        {
            this.abstractPricingTierDao = abstractPricingTierDao;
        }

        public override SuccessResult<AbstractPricingTier> PricingTier_Upsert(AbstractPricingTier AbstractPricingTier)
        {
            return abstractPricingTierDao.PricingTier_Upsert(AbstractPricingTier);
        }

        public override SuccessResult<AbstractPricingTier> PricingTier_ById(int Id)
        {
            return abstractPricingTierDao.PricingTier_ById(Id);
        }

        public override PagedList<AbstractPricingTier> PricingTier_All(PageParam pageParam, string search, AbstractPricingTier AbstractPricingTier = null)
        {
            return abstractPricingTierDao.PricingTier_All(pageParam, search, AbstractPricingTier);
        }

        public override SuccessResult<AbstractPricingTier> PricingTier_Delete(int Id)
        {
            return this.abstractPricingTierDao.PricingTier_Delete(Id);
        }
    }
}
