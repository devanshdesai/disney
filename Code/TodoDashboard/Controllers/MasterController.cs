﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TodoDashboard.Common.Paging;
using TodoDashboard.Infrastructure;
using TodoDashboard.Pages;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;
using TodoDashboard.Services.Contract;
using TodoDashboard.Common;
using System.Data;
using System.Data.SqlClient;
using OfficeOpenXml.Table;
using OfficeOpenXml;

namespace TodoDashboard.Controllers
{
    public class MasterController : BaseController
    {
        #region Fields
        private readonly AbstractMasterWineVaritalsService abstractMasterWineVaritalsService;
        private readonly AbstractMasterBarTypesService abstractMasterBarTypesService;
        private readonly AbstractMasterServicesTypesService abstractMasterServicesTypesService;
        private readonly AbstractMasterTiersService abstractMasterTiersService;
        private readonly AbstractMasterDistributorsService abstractMasterDistributorsService;
        private readonly AbstractMasterServingSizeServices abstractMasterServingSizeServices;
        private readonly AbstractMasterLevelServices abstractMasterLevelServices;
        private readonly AbstractMasterSuppliersServices abstractMasterSuppliersServices;
        #endregion

        #region Ctor
        public MasterController(AbstractMasterWineVaritalsService abstractMasterWineVaritalsService, 
            AbstractMasterBarTypesService abstractMasterBarTypesService,
            AbstractMasterServicesTypesService abstractMasterServicesTypesService, 
            AbstractMasterTiersService abstractMasterTiersService,
            AbstractMasterDistributorsService abstractMasterDistributorsService,
            AbstractMasterServingSizeServices abstractMasterServingSizeServices,
            AbstractMasterLevelServices abstractMasterLevelServices,
            AbstractMasterSuppliersServices abstractMasterSuppliersServices)
        {
            this.abstractMasterWineVaritalsService=abstractMasterWineVaritalsService;
            this.abstractMasterBarTypesService=abstractMasterBarTypesService;
            this.abstractMasterServicesTypesService=abstractMasterServicesTypesService;
            this.abstractMasterTiersService=abstractMasterTiersService;
            this.abstractMasterDistributorsService=abstractMasterDistributorsService;
            this.abstractMasterServingSizeServices=abstractMasterServingSizeServices;
            this.abstractMasterLevelServices = abstractMasterLevelServices;
            this.abstractMasterSuppliersServices = abstractMasterSuppliersServices;
        }
        #endregion

        #region Methods


        #region Wine Varitals
        [ActionName(Actions.WineVaritals)]
        public ActionResult WineVaritals()
        {
            return View();
        }
        #endregion

        #region Bar Types
        [ActionName(Actions.BarTypes)]
        public ActionResult BarTypes()
        {
            return View();
        }

        #endregion

        #region Service Types
        [ActionName(Actions.ServiceTypes)]
        public ActionResult ServiceTypes()
        {
            return View();
        }
        #endregion

        #region Tiers
        [ActionName(Actions.Tiers)]
        public ActionResult Tiers()
        {
            return View();
        }
        #endregion

        #region MasterServingSize
        [ActionName(Actions.MasterServingSize)]
        public ActionResult MasterServingSize()
        {
            return View();
        }
        #endregion

        #region Distributors
        [ActionName(Actions.Distributors)]
        public ActionResult Distributors()
        {
            return View();
        }
        #endregion

        #region Level
        [ActionName(Actions.Level)]
        public ActionResult Level()
        {
            return View();
        }
        #endregion

        #region Suppliers
        [ActionName(Actions.Suppliers)]
        public ActionResult Suppliers()
        {
            return View();
        }
        #endregion

        #region General
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string action = "")
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=requestModel.Start;
                pageParam.Limit=requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);

                if(action=="winevaritals")
                {
                    var model = abstractMasterWineVaritalsService.MasterWineVaritals_All(pageParam, search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                else if(action=="bartypes")
                {
                    var model = abstractMasterBarTypesService.MasterBarTypes_All(pageParam, search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                else if(action=="servicetypes")
                {
                    var model = abstractMasterServicesTypesService.MasterServicesTypes_All(pageParam, search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                else if(action=="tiers")
                {
                    var model = abstractMasterTiersService.MasterTiers_All(pageParam, search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                else if(action=="MasterServingSize")
                {
                    var model = abstractMasterServingSizeServices.MasterServingSize_All(pageParam, search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                else if(action=="distributors")
                {
                    var model = abstractMasterDistributorsService.MasterDistributors_All(pageParam, search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                else if (action == "Level")
                {
                    var model = abstractMasterLevelServices.MasterLevel_All(pageParam, search);
                    totalRecord = (int)model.TotalRecords;
                    filteredRecord = (int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }

                else if (action == "Suppliers")
                {
                    var model = abstractMasterSuppliersServices.MasterSuppliers_All(pageParam, search);
                    totalRecord = (int)model.TotalRecords;
                    filteredRecord = (int)model.TotalRecords;
                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }

                return Json(new DataTablesResponse(requestModel.Draw, null, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetMasterId(string Search = "", string action = "")
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;
                

                if(action=="winevaritals")
                {
                    var model = abstractMasterWineVaritalsService.MasterWineVaritals_All(pageParam, Search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    string ids = "";
                    for(int i = 0; i<model.Values.Count; i++)
                    {
                        if(i==0)
                        {
                            ids=model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids=ids+","+model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }
                else if(action=="bartypes")
                {
                    var model = abstractMasterBarTypesService.MasterBarTypes_All(pageParam, Search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    string ids = "";
                    for(int i = 0; i<model.Values.Count; i++)
                    {
                        if(i==0)
                        {
                            ids=model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids=ids+","+model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }
                else if(action=="servicetypes")
                {
                    var model = abstractMasterServicesTypesService.MasterServicesTypes_All(pageParam, Search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    string ids = "";
                    for(int i = 0; i<model.Values.Count; i++)
                    {
                        if(i==0)
                        {
                            ids=model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids=ids+","+model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }
                else if(action=="tiers")
                {
                    var model = abstractMasterTiersService.MasterTiers_All(pageParam, Search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    string ids = "";
                    for(int i = 0; i<model.Values.Count; i++)
                    {
                        if(i==0)
                        {
                            ids=model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids=ids+","+model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }
                else if(action=="MasterServingSize")
                {
                    var model = abstractMasterServingSizeServices.MasterServingSize_All(pageParam, Search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    string ids = "";
                    for(int i = 0; i<model.Values.Count; i++)
                    {
                        if(i==0)
                        {
                            ids=model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids=ids+","+model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }
                else if(action=="distributors")
                {
                    var model = abstractMasterDistributorsService.MasterDistributors_All(pageParam, Search);
                    totalRecord=(int)model.TotalRecords;
                    filteredRecord=(int)model.TotalRecords;
                    string ids = "";
                    for(int i = 0; i<model.Values.Count; i++)
                    {
                        if(i==0)
                        {
                            ids=model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids=ids+","+model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }

                else if (action == "Level")
                {
                    var model = abstractMasterLevelServices.MasterLevel_All(pageParam, Search);
                    totalRecord = (int)model.TotalRecords;
                    filteredRecord = (int)model.TotalRecords;
                    string ids = "";
                    for (int i = 0; i < model.Values.Count; i++)
                    {
                        if (i == 0)
                        {
                            ids = model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids = ids + "," + model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }

                else if (action == "Suppliers")
                {
                    var model = abstractMasterSuppliersServices.MasterSuppliers_All(pageParam, Search);
                    totalRecord = (int)model.TotalRecords;
                    filteredRecord = (int)model.TotalRecords;
                    string ids = "";
                    for (int i = 0; i < model.Values.Count; i++)
                    {
                        if (i == 0)
                        {
                            ids = model.Values[i].Id.ToString();
                        }
                        else
                        {
                            ids = ids + "," + model.Values[i].Id.ToString();
                        }
                    }

                    return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
                }



                return Json(new { Code = 200, Message = "Data Retrive Successfully"}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Upsert(int Id, string name, string action)
        {
            if(action=="winevaritals")
            {
                AbstractMasterWineVaritals abstractMasterWineVaritals = new MasterWineVaritals();
                abstractMasterWineVaritals.Id=Id;
                abstractMasterWineVaritals.Name=name;
                SuccessResult<AbstractMasterWineVaritals> successResult = abstractMasterWineVaritalsService.MasterWineVaritals_Upsert(abstractMasterWineVaritals);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="bartypes")
            {
                AbstractMasterBarTypes abstractMasterBarTypes = new MasterBarTypes();
                abstractMasterBarTypes.Id=Id;
                abstractMasterBarTypes.Name=name;
                SuccessResult<AbstractMasterBarTypes> successResult = abstractMasterBarTypesService.MasterBarTypes_Upsert(abstractMasterBarTypes);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="servicetypes")
            {
                AbstractMasterServicesTypes abstractMasterServicesTypes = new MasterServicesTypes();
                abstractMasterServicesTypes.Id=Id;
                abstractMasterServicesTypes.Name=name;
                SuccessResult<AbstractMasterServicesTypes> successResult = abstractMasterServicesTypesService.MasterServicesTypes_Upsert(abstractMasterServicesTypes);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="tiers")
            {
                AbstractMasterTiers abstractMasterTiers = new MasterTiers();
                abstractMasterTiers.Id=Id;
                abstractMasterTiers.Name=name;
                SuccessResult<AbstractMasterTiers> successResult = abstractMasterTiersService.MasterTiers_Upsert(abstractMasterTiers);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="MasterServingSize")
            {
                AbstractMasterServingSize model = new MasterServingSize();
                model.Id=Id;
                model.Name=name;
                SuccessResult<AbstractMasterServingSize> successResult = abstractMasterServingSizeServices.MasterServingSize_Upsert(model);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="distributors")
            {
                SuccessResult<AbstractMasterDistributors> successResult = null;
                if(Id>0)
                {
                    AbstractMasterDistributors abstractMasterDistributors = new MasterDistributors();
                    abstractMasterDistributors.Id=Id;
                    abstractMasterDistributors.Name=name;
                    successResult=abstractMasterDistributorsService.MasterDistributors_Upsert(abstractMasterDistributors);
                }
                else
                {
                    string[] Namearr = name.Split(',');
                    foreach(var names in Namearr)
                    {
                        AbstractMasterDistributors abstractMasterDistributors = new MasterDistributors();
                        abstractMasterDistributors.Name=names;
                        successResult=abstractMasterDistributorsService.MasterDistributors_Upsert(abstractMasterDistributors);
                    }
                }

                return Json(successResult, JsonRequestBehavior.AllowGet);
            }

            else if (action == "Level")
            {
                AbstractMasterLevel abstractMasterLevel = new MasterLevel();
                abstractMasterLevel.Id = Id;
                abstractMasterLevel.Name = name;
                SuccessResult<AbstractMasterLevel> successResult = abstractMasterLevelServices.MasterLevel_Upsert(abstractMasterLevel);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }

            else if (action == "Suppliers")
            {
                AbstractMasterSuppliers abstractMasterSuppliers = new MasterSuppliers();
                abstractMasterSuppliers.Id = Id;
                abstractMasterSuppliers.Name = name;
                SuccessResult<AbstractMasterSuppliers> successResult = abstractMasterSuppliersServices.MasterSuppliers_Upsert(abstractMasterSuppliers);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }

            return Json("400", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ExportExcel(string ids = "", string action = "")
        {
            //string[] idsArr = ids.Split(',');
            //string tableName = "";
            //if(action=="winevaritals")
            //{
            //    tableName="MasterWineVaritals";
            //}
            //else if(action=="bartypes")
            //{
            //    tableName="MasterBarTypes";
            //}
            //else if(action=="servicetypes")
            //{
            //    tableName="MasterServicesTypes";
            //}
            //else if(action=="tiers")
            //{
            //    tableName="MasterTiers";

            //}
            //else if(action=="distributors")
            //{
            //    tableName="MasterDistributors";

            //}
            //else if(action=="MasterServingSize")
            //{
            //    tableName="MasterServingSize";

            //}

            //string query = "";
            //if(idsArr[0]=="All"&&idsArr.Length>1)
            //{
            //    ids=ids.Replace("All,", "");
            //    query=$"select * from {tableName} where id not in ({ids})";
            //}
            //else if(idsArr[0]=="All"||(string.IsNullOrEmpty(idsArr[0])&&idsArr.Length==1))
            //{
            //    query=$"select * from {tableName}";
            //}
            //else
            //{
            //    query=$"select * from {tableName} where id  in ({ids})";
            //}
            //if(string.IsNullOrEmpty(ids))
            //{
            //    ids="0";
            //}
            //using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            //{
            //    SqlDataAdapter ad = null;
            //    DataTable dt = new DataTable();
            //    ad=new SqlDataAdapter(query, con);
            //    ad.Fill(dt);
            //    for(int i = 0; i<dt.Rows.Count; i++)
            //    {
            //        if(i==0)
            //        {
            //            ids=dt.Rows[i]["Id"].ToString();
            //        }
            //        else
            //        {
            //            ids=ids+","+dt.Rows[i]["Id"].ToString();
            //        }
            //    }
            //}

            
            string var_sql = "";
            if(action=="winevaritals")
            {
                var_sql="select Name from MasterWineVaritals ";
            }
            else if(action=="bartypes")
            {
                var_sql="select Name from MasterBarTypes";
                
            }
            else if(action=="servicetypes")
            {
                var_sql="select Name from MasterServicesTypes";

            }
            else if(action=="tiers")
            {
                var_sql="select Name from MasterTiers";
            }

            else if (action == "Level")
            {
                var_sql = "select Name from MasterLevel";
            }

            else if (action == "Suppliers")
            {
                var_sql = "select Name from MasterSuppliers";
            }

            else if(action=="distributors")
            {
                var_sql="select Name from MasterDistributors ";
            }
            else if(action=="MasterServingSize")
            {
                var_sql="select Name from MasterServingSize ";
            }

            if(!string.IsNullOrEmpty(ids))
            {
                var_sql=var_sql+" where Id in ("+ids+")";
            }
            try
            {
                ExcelPackage.LicenseContext=LicenseContext.NonCommercial;
                using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter(var_sql, con);
                    DataTable dt = new DataTable();
                    ad.Fill(dt);
                    using(var excelPackage = new ExcelPackage())
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.Add("sheet 1");
                        worksheet.Cells["A1"].LoadFromDataTable(dt, true, TableStyles.None);
                        Session["DownloadExcel_FileManager"]=excelPackage.GetAsByteArray();
                    }
                }

                return Json(new { Code = 200, Message = "File Created Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { Code = 400, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Delete(string ids, string action)
        {
            try
            {
                if(string.IsNullOrEmpty(ids))
                {
                    throw new Exception("");
                }
                string var_sql = "";
                if(action=="winevaritals")
                {
                    var_sql="delete from MasterWineVaritals where Id in ("+ids+")";
                }
                else if(action=="bartypes")
                {
                    var_sql="delete from MasterBarTypes where Id in ("+ids+")";
                }
                else if(action=="servicetypes")
                {
                    var_sql="delete from MasterServicesTypes where Id in ("+ids+")";
                }
                else if(action=="tiers")
                {
                    var_sql="delete from MasterTiers where Id in ("+ids+")";
                }

                else if (action == "Level")
                {
                    var_sql = "delete from MasterLevel where Id in (" + ids + ")";
                }

                else if (action == "Suppliers")
                {
                    var_sql = "delete from MasterSuppliers where Id in (" + ids + ")";
                }

                else if(action=="distributors")
                {
                    var_sql="delete from MasterDistributors where Id in ("+ids+")";
                }
                else if(action=="MasterServingSize")
                {
                    var_sql="delete from MasterServingSize where Id in ("+ids+")";
                }
                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetById(int Id, string action)
        {
            if(action=="winevaritals")
            {
                SuccessResult<AbstractMasterWineVaritals> successResult = abstractMasterWineVaritalsService.MasterWineVaritals_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="bartypes")
            {
                SuccessResult<AbstractMasterBarTypes> successResult = abstractMasterBarTypesService.MasterBarTypes_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="servicetypes")
            {
                SuccessResult<AbstractMasterServicesTypes> successResult = abstractMasterServicesTypesService.MasterServicesTypes_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="tiers")
            {
                SuccessResult<AbstractMasterTiers> successResult = abstractMasterTiersService.MasterTiers_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="distributors")
            {
                SuccessResult<AbstractMasterDistributors> successResult = abstractMasterDistributorsService.MasterDistributors_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }
            else if(action=="MasterServingSize")
            {
                SuccessResult<AbstractMasterServingSize> successResult = abstractMasterServingSizeServices.MasterServingSize_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }

            else if (action == "Level")
            {
                SuccessResult<AbstractMasterLevel> successResult = abstractMasterLevelServices.MasterLevel_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }

            else if (action == "Suppliers")
            {
                SuccessResult<AbstractMasterSuppliers> successResult = abstractMasterSuppliersServices.MasterSuppliers_Id(Id);
                return Json(successResult, JsonRequestBehavior.AllowGet);
            }

            return Json("200", JsonRequestBehavior.AllowGet);
        }
        #endregion



        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        //{
        //    try
        //    {
        //        int totalRecord = 0;
        //        int filteredRecord = 0;

        //        PageParam pageParam = new PageParam();
        //        pageParam.Offset = requestModel.Start;
        //        pageParam.Limit = requestModel.Length;

        //        List<Drinks> drinks = new List<Drinks>();

        //        Drinks Drinks = new Drinks();
        //        Drinks.Name = "Bacardi Mojito";
        //        Drinks.Spirit = "Rum";
        //        Drinks.Brand = "Bacardi Superior";
        //        Drinks.Ingredients = "Lime Wedges 2 each (in drinks as garnish)"+"<br/>"+"Mini Leaves 4 each";
        //        Drinks.Garnish = "Lime Wheel, Mint Spring";
        //        Drinks.Glassware = "Collins";
        //        Drinks.Price = "13";
        //        Drinks.Location = "Akershus"+"<br/>"+"Ale & Compass"+ "<br/>" + "Boma";
        //        Drinks.LoungeLevel = "Signature";
        //        Drinks.Seasonal = "Domestic"+ "<br/>" + "Resort"+ "<br/>" + "Moderate";
        //        Drinks.Photo = "../assets/images/xs/wine.png";

        //        drinks.Add(Drinks);


        //        Drinks = new Drinks();
        //        Drinks.Name = "Banana Spiced Rum";
        //        Drinks.Spirit = "Rum";
        //        Drinks.Brand = "Captain" + "<br/>" + "Morgon" + "<br/>" + "Original" + "<br/>" + "Spiced";
        //        Drinks.Ingredients = "RumChata 1 oz" + "<br/>" + "Captain Morgon Original"+"<br/>"+"Spiced Rum 1 oz"+"<br/>"+"BOLS Creme de B anana 1 oz";
        //        Drinks.Garnish = "None";
        //        Drinks.Glassware = "Martini";
        //        Drinks.Price = "13";
        //        Drinks.Location = "Hollywood & Vine" + "<br/>" + "Marana Melrose's" + "<br/>" + "Olieia's Cafe";
        //        Drinks.LoungeLevel = "Moderate";
        //        Drinks.Seasonal = "Domestic" + "<br/>" + "Resort" + "<br/>" + "Luxury";
        //        Drinks.Photo = "../assets/images/xs/wine.png";

        //        drinks.Add(Drinks);


        //        Drinks = new Drinks();
        //        Drinks.Name = "Bacardi Mojito";
        //        Drinks.Spirit = "Rum";
        //        Drinks.Brand = "Bacardi Superior";
        //        Drinks.Ingredients = "Lime Wedges 2 each (in drinks as garnish)" + "<br/>" + "Mini Leaves 4 each";
        //        Drinks.Garnish = "Lime Wheel, Mint Spring";
        //        Drinks.Glassware = "Collins";
        //        Drinks.Price = "13";
        //        Drinks.Location = "Akershus" + "<br/>" + "Ale & Compass" + "<br/>" + "Boma";
        //        Drinks.LoungeLevel = "Signature";
        //        Drinks.Seasonal = "Domestic" + "<br/>" + "Resort" + "<br/>" + "Moderate";
        //        Drinks.Photo = "../assets/images/xs/wine.png";

        //        drinks.Add(Drinks);

        //        string search = requestModel.Search.Value;
        //        //var model = null;
        //        totalRecord = (int)3;
        //        filteredRecord = (int)3;

        //        return Json(new DataTablesResponse(requestModel.Draw, drinks, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        //ErrorLogHelper.Log(ex);
        //        return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult AddDrink()
        //{
        //    return View();
        //}

        //public ActionResult EditDrink()
        //{
        //    return View();
        //}

        //public ActionResult ViewDrink()
        //{
        //    return View();
        //}
        #endregion
    }

    public class MasterControllerDrinks
    {
        public int Id;
        public string Name;
        public string Spirit;
        public string Brand;
        public string Ingredients;
        public string Garnish;
        public string Glassware;
        public string Price;
        public string Location;
        public string LoungeLevel;
        public string Seasonal;
        public string Photo;
    }
}