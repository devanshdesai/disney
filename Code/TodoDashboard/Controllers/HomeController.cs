﻿using DataTables.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;
using TodoDashboard.Entities.V1;
using TodoDashboard.Infrastructure;
using TodoDashboard.Pages;
using TodoDashboard.Services.Contract;


namespace TodoDashboard.Controllers
{
    public class HomeController : BaseController
    {
        #region Fields
        public readonly AbstractMasterLocationsService abstractMasterLocationsService;
        public readonly AbstractMasterWinesService abstractMasterWinesService;
        private readonly AbstractMasterWineVaritalsService abstractMasterWineVaritalsService;
        private readonly AbstractMasterBarTypesService abstractMasterBarTypesService;
        private readonly AbstractMasterServicesTypesService abstractMasterServicesTypesService;
        private readonly AbstractMasterTiersService abstractMasterTiersService;
        private readonly AbstractMasterDistributorsService abstractMasterDistributorsService;
        private readonly AbstractMasterStatesService abstractMasterStatesService;
        private readonly AbstractPricingTierServices abstractPricingTierServices;
        private readonly AbstractMasterLevelServices abstractMasterLevelServices;
        private readonly AbstractMasterSuppliersServices abstractMasterSuppliersServices;
        #endregion

        #region Ctor
        public HomeController(AbstractMasterLocationsService abstractMasterLocationsService, AbstractMasterWinesService abstractMasterWinesService,
            AbstractMasterWineVaritalsService abstractMasterWineVaritalsService, AbstractMasterBarTypesService abstractMasterBarTypesService
            , AbstractMasterServicesTypesService abstractMasterServicesTypesService, AbstractMasterTiersService abstractMasterTiersService,
            AbstractMasterDistributorsService abstractMasterDistributorsService, AbstractMasterStatesService abstractMasterStatesService,
            AbstractPricingTierServices abstractPricingTierServices, AbstractMasterLevelServices abstractMasterLevelServices,
            AbstractMasterSuppliersServices abstractMasterSuppliersServices)
        {
            this.abstractMasterLocationsService=abstractMasterLocationsService;
            this.abstractMasterWinesService=abstractMasterWinesService;
            this.abstractMasterWineVaritalsService=abstractMasterWineVaritalsService;
            this.abstractMasterBarTypesService=abstractMasterBarTypesService;
            this.abstractMasterServicesTypesService=abstractMasterServicesTypesService;
            this.abstractMasterTiersService=abstractMasterTiersService;
            this.abstractMasterDistributorsService=abstractMasterDistributorsService;
            this.abstractMasterStatesService=abstractMasterStatesService;
            this.abstractPricingTierServices = abstractPricingTierServices;
            this.abstractMasterSuppliersServices = abstractMasterSuppliersServices;
            this.abstractMasterLevelServices = abstractMasterLevelServices;
        }
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            ViewBag.LocationIds=JsonConvert.SerializeObject(GetValues("LocationIds"));
            ViewBag.LocationName=JsonConvert.SerializeObject(GetValues("LocationName"));
            ViewBag.WineName=JsonConvert.SerializeObject(GetValues("WineName"));
            ViewBag.States=BindDropDowns("states");
            ViewBag.ServiceTypes=BindDropDowns("servicetypes");
            ViewBag.BarTypes=BindDropDowns("bartypes");
            ViewBag.WineVaritals=BindDropDowns("winevaritals");
            ViewBag.Distributors=BindDropDowns("distributors");
            ViewBag.Tiers=BindDropDowns("tiers");
            ViewBag.MasterWines = BindDropDowns("masterwines");
            ViewBag.MasterLevel = BindDropDowns("masterlevel");
            ViewBag.MasterSuppliers = BindDropDowns("mastersuppliers");
            return View();
        }

        public ActionResult SubmitForm()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindPricingTierData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int MasterWineId = 0, int MasterWineVaritalId = 0, int MasterDistributorId = 0, int MasterSupplierId = 0, int MasterLevelId = 0, int MasterTierId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractPricingTier abstractPricingTier = new PricingTier();
                abstractPricingTier.MasterWineId = MasterWineId;
                abstractPricingTier.MasterWineVaritalId = MasterWineVaritalId;
                abstractPricingTier.MasterDistributorId = MasterDistributorId;
                abstractPricingTier.MasterSupplierId = MasterSupplierId;
                abstractPricingTier.MasterLevelId = MasterLevelId;
                abstractPricingTier.MasterTierId = MasterTierId;

                var model = abstractPricingTierServices.PricingTier_All(pageParam, search, abstractPricingTier);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;


                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindLocationData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string LocationId = "", string Location = "", int StateId = 0, int ServiceTypeId = 0, int BarTypeId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=requestModel.Start;
                pageParam.Limit=requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                abstractMasterLocations.LocationId=LocationId;
                abstractMasterLocations.LocationName=Location;
                abstractMasterLocations.StateId=StateId;
                abstractMasterLocations.ServiceTypeId=ServiceTypeId;
                abstractMasterLocations.BarTypeId=BarTypeId;

                var model = abstractMasterLocationsService.MasterLocations_All(pageParam, search, abstractMasterLocations);
                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;


                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetLocationId(string Search = "",string LocationId = "", string Location = "", int StateId = 0, int ServiceTypeId = 0, int BarTypeId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;

                string search = Convert.ToString(Search);

                AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                abstractMasterLocations.LocationId=LocationId;
                abstractMasterLocations.LocationName=Location;
                abstractMasterLocations.StateId=StateId;
                abstractMasterLocations.ServiceTypeId=ServiceTypeId;
                abstractMasterLocations.BarTypeId=BarTypeId;

                var model = abstractMasterLocationsService.MasterLocations_All(pageParam, search, abstractMasterLocations);
                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;

                string ids = "";
                for(int i = 0; i<model.Values.Count; i++)
                {
                    if(i==0)
                    {
                        ids=model.Values[i].Id.ToString();
                    }
                    else
                    {
                        ids=ids+","+model.Values[i].Id.ToString();
                    }
                }
                
                return Json(new { Code = 200,Message = "Data Retrive Successfully",data = ids}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetWineId(string Search = "",string WineName = "", int VarietalId = 0, string Calories = "", int DistributorId = 0, int TierId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;


                AbstractMasterWines abstractMasterWines = new MasterWines();
                abstractMasterWines.WineName=WineName;
                abstractMasterWines.MasterVarietalId=VarietalId;
                abstractMasterWines.Calories=Calories;
                abstractMasterWines.MasterDistributorId=DistributorId;
                abstractMasterWines.TierId=TierId;

                var model = abstractMasterWinesService.MasterWines_All(pageParam, Search, abstractMasterWines);
                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;

                string ids = "";
                for(int i = 0; i<model.Values.Count; i++)
                {
                    if(i==0)
                    {
                        ids=model.Values[i].Id.ToString();
                    }
                    else
                    {
                        ids=ids+","+model.Values[i].Id.ToString();
                    }
                }

                return Json(new { Code = 200, Message = "Data Retrive Successfully", data = ids }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ExportExcel(string ids = "", string Refrence = "", bool IsDetailRefrence = false)
        {
            ExcelPackage.LicenseContext=LicenseContext.NonCommercial;
            
            
            if(Refrence.ToLower().CompareTo("location")==0)
            {
                

                using(var excelPackage = new ExcelPackage())
                {

                    var worksheet = excelPackage.Workbook.Worksheets.Add("sheet 1");

                    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                    {
                        string Query = $"select ML.Id AS 'LocId',LocationId AS 'Id',LocationName  AS 'Location',MS.Name AS 'State',St.Name as 'Service Type',BT.Name AS 'Bar Type' from MasterLocations ML LEFT JOIN MasterStates MS ON ML.StateId =  MS.Id LEFT JOIN MasterServicesTypes ST ON ML.ServiceTypeId = ST.Id LEFT JOIN MasterBarTypes BT ON ML.BarTypeId = BT.Id";
                        if(!string.IsNullOrEmpty(ids))
                        {
                            Query=Query+$" WHERE ML.Id in ({ ids})";
                        }

                        DataTable dtExcel = new DataTable();

                        DataTable dtLocation = new DataTable();
                        SqlDataAdapter adLocation = new SqlDataAdapter(Query, con);
                        adLocation.Fill(dtLocation);

                        Query=$"select LW.MasterLocationId AS 'LocId', WineName AS 'Wine Name',MV.Name AS 'Varietals Name',Calories AS 'Calories',SS.Name AS 'Serving Size',ISNULL(MD.Name,'') AS 'Distributor',ISNULL(MT.Name,'') AS 'Tier'  from LocationWines LW LEFT JOIN MasterWines MW ON LW.MasterWineId = MW.Id LEFT JOIN MasterServingSize SS ON LW.MasterServingSizeId = SS.Id LEFT JOIN MasterDistributors MD ON MW.MasterDistributorId = MD.Id LEFT JOIN MasterTiers MT ON MW.TierId = MT.Id LEFT JOIN MasterWineVaritals MV ON MW.MasterVarietalId = MV.Id";
                        if(!string.IsNullOrEmpty(ids))
                        {
                            Query=Query+$" WHERE LW.MasterLocationId IN ({ids})";
                        }
                        DataTable dtWine = new DataTable();
                        SqlDataAdapter adWine = new SqlDataAdapter(Query, con);
                        adWine.Fill(dtWine);

                        foreach(DataColumn item in dtLocation.Columns)
                        {
                            dtExcel.Columns.Add(item.ColumnName.ToString());
                        }

                        if(IsDetailRefrence==true)
                        {
                        

                            foreach(DataColumn item in dtWine.Columns)
                            {
                                DataColumnCollection columns = dtExcel.Columns;
                                if(!dtExcel.Columns.Contains(item.ColumnName.ToString()))
                                    dtExcel.Columns.Add(item.ColumnName.ToString());
                            }
                        }
                        for(int i = 0; i<dtLocation.Rows.Count; i++)
                        {
                            if(IsDetailRefrence == true)
                            {
                                dtExcel.Rows.Add();
                                bool IsInserted = false;
                                bool IsFound = false;
                                for (int j = 0; j<dtWine.Rows.Count; j++)
                                {

                                    if(dtWine.Rows[j]["LocId"].ToString().CompareTo(dtLocation.Rows[i]["LocId"].ToString())==0)
                                    {
                                        IsFound = true;
                                        DataRow row = dtExcel.NewRow();

                                        if(IsInserted==false)
                                        {
                                            row["Id"]=dtLocation.Rows[i]["Id"].ToString();
                                            row["Location"]=dtLocation.Rows[i]["Location"].ToString();
                                            row["State"]=dtLocation.Rows[i]["State"].ToString();
                                            row["Service Type"]=dtLocation.Rows[i]["Service Type"].ToString();
                                            row["Bar Type"]=dtLocation.Rows[i]["Bar Type"].ToString();
                                            IsInserted=true;
                                        }
                                        row["Wine Name"]=dtWine.Rows[j]["Wine Name"].ToString();
                                        row["Varietals Name"]=dtWine.Rows[j]["Varietals Name"].ToString();
                                        row["Calories"]=dtWine.Rows[j]["Calories"].ToString();
                                        row["Serving Size"]=dtWine.Rows[j]["Serving Size"].ToString();
                                        row["Distributor"]=dtWine.Rows[j]["Distributor"].ToString();
                                        row["Tier"]=dtWine.Rows[j]["Tier"].ToString();
                                        dtExcel.Rows.Add(row);
                                    }
                                }
                                if (IsFound == false)
                                {
                                    DataRow row = dtExcel.NewRow();
                                    row["Id"] = dtLocation.Rows[i]["Id"].ToString();
                                    row["Location"] = dtLocation.Rows[i]["Location"].ToString();
                                    row["State"] = dtLocation.Rows[i]["State"].ToString();
                                    row["Service Type"] = dtLocation.Rows[i]["Service Type"].ToString();
                                    row["Bar Type"] = dtLocation.Rows[i]["Bar Type"].ToString();
                                    dtExcel.Rows.Add(row);
                                }
                            }
                            else
                            {
                                DataRow row = dtExcel.NewRow();
                                row["Id"]=dtLocation.Rows[i]["Id"].ToString();
                                row["Location"]=dtLocation.Rows[i]["Location"].ToString();
                                row["State"]=dtLocation.Rows[i]["State"].ToString();
                                row["Service Type"]=dtLocation.Rows[i]["Service Type"].ToString();
                                row["Bar Type"]=dtLocation.Rows[i]["Bar Type"].ToString();
                                dtExcel.Rows.Add(row);
                            }
                        }
                        dtExcel.Columns.Remove("LocId");
                        worksheet.Cells["A1"].LoadFromDataTable(dtExcel, true, TableStyles.None);

                        Session["DownloadExcel_FileManager"]=excelPackage.GetAsByteArray();
                    }

                }
                return Json(new { Code = 200, Message = "File Created Successfully" });
            }
            else if(Refrence.ToLower().CompareTo("wine")==0)
            {
                using(var excelPackage = new ExcelPackage())
                {
                    var worksheet = excelPackage.Workbook.Worksheets.Add("sheet 1");

                    using(SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                    {
                        string Query = $"select MW.Id AS 'WineId', WineName AS 'Wine Name',MV.Name AS 'Varietals Name',Calories AS 'Calories',ISNULL(MD.Name,'') AS 'Distributor',ISNULL(MT.Name,'') AS 'Tier' from MasterWines MW LEFT JOIN MasterDistributors MD ON MW.MasterDistributorId = MD.Id LEFT JOIN MasterTiers MT ON MW.TierId = MT.Id LEFT JOIN MasterWineVaritals MV ON MW.MasterVarietalId = MV.Id ";
                        if(!string.IsNullOrEmpty(ids))
                        {
                            Query=Query+$" WHERE MW.Id = ({ids})";
                        }

                        DataTable dtExcel = new DataTable();


                        DataTable dtWine = new DataTable();
                        SqlDataAdapter adWine = new SqlDataAdapter(Query, con);
                        adWine.Fill(dtWine);

                        Query=$"select LW.MasterWineId AS 'WineId',ML.LocationId AS 'Id',ML.LocationName AS 'Location',SS.Name AS 'Serving Size',MS.Name AS 'State',ST.Name AS 'Service Type',BT.Name AS 'Bar Type' from LocationWines LW  LEFT JOIN MasterLocations ML ON LW.MasterLocationId = ML.Id  LEFT JOIN MasterStates MS ON MS.Id = ML.StateId LEFT JOIN MasterServicesTypes ST ON ML.ServiceTypeId = ST.Id LEFT JOIN MasterBarTypes BT ON ML.BarTypeId = BT.Id LEFT JOIN MasterServingSize SS ON SS.Id = LW.MasterServingSizeId ";
                        if(!string.IsNullOrEmpty(ids))
                        {
                            Query=Query+$" WHERE LW.MasterWineId IN ({ids})";
                        }
                        DataTable dtLocation = new DataTable();
                        SqlDataAdapter adLocation = new SqlDataAdapter(Query, con);
                        adLocation.Fill(dtLocation);

                        foreach(DataColumn item in dtWine.Columns)
                        {
                            dtExcel.Columns.Add(item.ColumnName.ToString());
                        }

                        if(IsDetailRefrence==true)
                        {
                            foreach(DataColumn item in dtLocation.Columns)
                            {
                                DataColumnCollection columns = dtExcel.Columns;
                                if(!dtExcel.Columns.Contains(item.ColumnName.ToString()))
                                    dtExcel.Columns.Add(item.ColumnName.ToString());
                            }
                        }
                        for(int i = 0; i<dtWine.Rows.Count; i++)
                        {
                            if(IsDetailRefrence==true)
                            {
                                dtExcel.Rows.Add();
                                bool IsInserted = false;
                                bool IsFound = false;
                                for(int j = 0; j<dtLocation.Rows.Count; j++)
                                {

                                    if(dtLocation.Rows[j]["WineId"].ToString()==dtWine.Rows[i]["WineId"].ToString())
                                    {
                                        IsFound = true;
                                        DataRow row = dtExcel.NewRow();

                                        if(IsInserted==false)
                                        {
                                            row["Wine Name"]=dtWine.Rows[i]["Wine Name"].ToString();
                                            row["Varietals Name"]=dtWine.Rows[i]["Varietals Name"].ToString();
                                            row["Calories"]=dtWine.Rows[i]["Calories"].ToString();
                                            
                                            row["Distributor"]=dtWine.Rows[i]["Distributor"].ToString();
                                            row["Tier"]=dtWine.Rows[i]["Tier"].ToString();
                                            
                                            IsInserted=true;
                                        }
                                        row["Id"]=dtLocation.Rows[j]["Id"].ToString();
                                        row["Location"]=dtLocation.Rows[j]["Location"].ToString();
                                        row["Serving Size"]=dtLocation.Rows[j]["Serving Size"].ToString();
                                        row["State"]=dtLocation.Rows[j]["State"].ToString();
                                        row["Service Type"]=dtLocation.Rows[j]["Service Type"].ToString();
                                        row["Bar Type"]=dtLocation.Rows[j]["Bar Type"].ToString();
                                        dtExcel.Rows.Add(row);
                                    }
                                }
                                if(IsFound == false)
                                {
                                    DataRow row = dtExcel.NewRow();
                                    row["Wine Name"] = dtWine.Rows[i]["Wine Name"].ToString();
                                    row["Varietals Name"] = dtWine.Rows[i]["Varietals Name"].ToString();
                                    row["Calories"] = dtWine.Rows[i]["Calories"].ToString();

                                    row["Distributor"] = dtWine.Rows[i]["Distributor"].ToString();
                                    row["Tier"] = dtWine.Rows[i]["Tier"].ToString();
                                    dtExcel.Rows.Add(row);
                                }
                            }
                            else
                            {
                                DataRow row = dtExcel.NewRow();
                                row["Wine Name"]=dtWine.Rows[i]["Wine Name"].ToString();
                                row["Varietals Name"]=dtWine.Rows[i]["Varietals Name"].ToString();
                                row["Calories"]=dtWine.Rows[i]["Calories"].ToString();
                                
                                row["Distributor"]=dtWine.Rows[i]["Distributor"].ToString();
                                row["Tier"]=dtWine.Rows[i]["Tier"].ToString();
                                dtExcel.Rows.Add(row);
                            }
                        }
                        dtExcel.Columns.Remove("WineId");
                        worksheet.Cells["A1"].LoadFromDataTable(dtExcel, true, TableStyles.None);

                        Session["DownloadExcel_FileManager"]=excelPackage.GetAsByteArray();
                    }

                    //using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                    //{
                    //    string Query = $"select ML.Id AS 'LocId',LocationId AS 'Id',LocationName  AS 'Location',MS.Name AS 'State',St.Name as 'Service Type',BT.Name AS 'Bar Type' from MasterLocations ML LEFT JOIN MasterStates MS ON ML.StateId =  MS.Id LEFT JOIN MasterServicesTypes ST ON ML.ServiceTypeId = ST.Id LEFT JOIN MasterBarTypes BT ON ML.BarTypeId = BT.Id WHERE ML.Id in ({ids})";


                    //    DataTable dtExcel = new DataTable();

                    //    DataTable dtLocation = new DataTable();
                    //    SqlDataAdapter adLocation = new SqlDataAdapter(Query, con);
                    //    adLocation.Fill(dtLocation);

                    //    Query = $"select LW.MasterLocationId AS 'LocId', WineName AS 'Wine Name',MV.Name AS 'Varietals Name',Calories AS 'Calories',SS.Name AS 'Serving Size',ISNULL(MD.Name,'') AS 'Distributor',ISNULL(MT.Name,'') AS 'Tier'  from LocationWines LW LEFT JOIN MasterWines MW ON LW.MasterWineId = MW.Id LEFT JOIN MasterServingSize SS ON LW.MasterServingSizeId = SS.Id LEFT JOIN MasterDistributors MD ON MW.MasterDistributorId = MD.Id LEFT JOIN MasterTiers MT ON MW.TierId = MT.Id LEFT JOIN MasterWineVaritals MV ON MW.MasterVarietalId = MV.Id WHERE LW.MasterLocationId IN ({ids})";
                    //    DataTable dtWine = new DataTable();
                    //    SqlDataAdapter adWine = new SqlDataAdapter(Query, con);
                    //    adWine.Fill(dtWine);

                    //    foreach (DataColumn item in dtLocation.Columns)
                    //    {
                    //        dtExcel.Columns.Add(item.ColumnName.ToString());
                    //    }

                    //    if (IsDetailRefrence == true)
                    //    {


                    //        foreach (DataColumn item in dtWine.Columns)
                    //        {
                    //            DataColumnCollection columns = dtExcel.Columns;
                    //            if (!dtExcel.Columns.Contains(item.ColumnName.ToString()))
                    //                dtExcel.Columns.Add(item.ColumnName.ToString());
                    //        }
                    //    }
                    //    for (int i = 0; i < dtLocation.Rows.Count; i++)
                    //    {
                    //        if (IsDetailRefrence == true)
                    //        {
                    //            dtExcel.Rows.Add();
                    //            bool IsInserted = false;
                    //            for (int j = 0; j < dtWine.Rows.Count; j++)
                    //            {

                    //                if (dtWine.Rows[j]["LocId"].ToString().CompareTo(dtLocation.Rows[i]["LocId"].ToString()) == 0)
                    //                {
                    //                    DataRow row = dtExcel.NewRow();

                    //                    if (IsInserted == false)
                    //                    {
                    //                        row["Id"] = dtLocation.Rows[i]["Id"].ToString();
                    //                        row["Location"] = dtLocation.Rows[i]["Location"].ToString();
                    //                        row["State"] = dtLocation.Rows[i]["State"].ToString();
                    //                        row["Service Type"] = dtLocation.Rows[i]["Service Type"].ToString();
                    //                        row["Bar Type"] = dtLocation.Rows[i]["Bar Type"].ToString();
                    //                        IsInserted = true;
                    //                    }
                    //                    row["Wine Name"] = dtWine.Rows[j]["Wine Name"].ToString();
                    //                    row["Varietals Name"] = dtWine.Rows[j]["Varietals Name"].ToString();
                    //                    row["Calories"] = dtWine.Rows[j]["Calories"].ToString();
                    //                    row["Serving Size"] = dtWine.Rows[j]["Serving Size"].ToString();
                    //                    row["Distributor"] = dtWine.Rows[j]["Distributor"].ToString();
                    //                    row["Tier"] = dtWine.Rows[j]["Tier"].ToString();
                    //                    dtExcel.Rows.Add(row);
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            DataRow row = dtExcel.NewRow();
                    //            row["Id"] = dtLocation.Rows[i]["Id"].ToString();
                    //            row["Location"] = dtLocation.Rows[i]["Location"].ToString();
                    //            row["State"] = dtLocation.Rows[i]["State"].ToString();
                    //            row["Service Type"] = dtLocation.Rows[i]["Service Type"].ToString();
                    //            row["Bar Type"] = dtLocation.Rows[i]["Bar Type"].ToString();
                    //            dtExcel.Rows.Add(row);
                    //        }
                    //    }
                    //    dtExcel.Columns.Remove("LocId");
                    //    worksheet.Cells["A1"].LoadFromDataTable(dtExcel, true, TableStyles.None);

                    //    Session["DownloadExcel_FileManager"] = excelPackage.GetAsByteArray();
                    //}

                }
                return Json(new { Code = 200, Message = "File Created Successfully" });
            }
            else
            {
                return Json(new { Code = 400, Message = "Please try Again" });
            }
        }
        [HttpGet]
        public ActionResult Download()
        {
            byte[] data = Session["DownloadExcel_FileManager"] as byte[];
            return File(data, "application/octet-stream", "AMC Database.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindWineData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string WineName = "", int VarietalId = 0, string Calories = "", int DistributorId = 0, int TierId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset=requestModel.Start;
                pageParam.Limit=requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractMasterWines abstractMasterWines = new MasterWines();
                abstractMasterWines.WineName=WineName;
                abstractMasterWines.MasterVarietalId=VarietalId;
                abstractMasterWines.Calories=Calories;
                abstractMasterWines.MasterDistributorId=DistributorId;
                abstractMasterWines.TierId=TierId;

                var model = abstractMasterWinesService.MasterWines_All(pageParam, search, abstractMasterWines);
                filteredRecord=(int)model.TotalRecords;
                totalRecord=(int)model.TotalRecords;


                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Delete(string ids, string action)
        {
            

            string var_sql = "";
            string var_sql2 = "";
            if(action=="locations")
            {
                //if(string.IsNullOrEmpty(ids))
                //{
                    
                //    var_sql="delete from MasterLocations";
                //    var_sql2="delete from LocationWines";
                //}
                if(!string.IsNullOrEmpty(ids))
                {
                    var_sql="delete from MasterLocations where Id in ("+ids+")";
                    var_sql2="delete from LocationWines where MasterLocationId in ("+ids+")";
                }
            }
            else if(action=="wines")
            {
                //if(string.IsNullOrEmpty(ids))
                //{

                //    var_sql="delete from MasterWines";
                //    var_sql2="delete from LocationWines";
                //}
                if(!string.IsNullOrEmpty(ids))
                {
                    var_sql="delete from MasterWines where Id in ("+ids+")";
                    var_sql2="delete from LocationWines where MasterWineId in ("+ids+")";
                }
            }
            else if (action == "pricingtier")
            {
                //if(string.IsNullOrEmpty(ids))
                //{

                //    var_sql="delete from MasterWines";
                //    var_sql2="delete from LocationWines";
                //}
                if (!string.IsNullOrEmpty(ids))
                {
                    var_sql = "delete from PricingTier where Id in (" + ids + ")";
                }
            }
            try
            {
                if(!string.IsNullOrEmpty(ids))
                {
                    SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand(var_sql, con);
                    cmd.ExecuteNonQuery();
                    con.Close();

                    cmd=new SqlCommand(var_sql2, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                else
                {
                    throw new Exception("");
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset=0;
                pageParam.Limit=0;

                if(action=="winevaritals")
                {
                    var model = abstractMasterWineVaritalsService.MasterWineVaritals_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="bartypes")
                {
                    var model = abstractMasterBarTypesService.MasterBarTypes_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="servicetypes")
                {
                    var model = abstractMasterServicesTypesService.MasterServicesTypes_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="tiers")
                {
                    var model = abstractMasterTiersService.MasterTiers_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="distributors")
                {
                    var model = abstractMasterDistributorsService.MasterDistributors_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="states")
                {
                    var model = abstractMasterStatesService.MasterStates_All(pageParam, "");
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.Name.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if(action=="LocationIds")
                {
                    AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                    var model = abstractMasterLocationsService.MasterLocations_All(pageParam, "", abstractMasterLocations);
                    foreach(var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text=master.LocationId.ToString(), Value=Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterwines")
                {
                    AbstractMasterWines abstractMasterWines = new MasterWines();
                    var model = abstractMasterWinesService.MasterWines_All(pageParam, "", abstractMasterWines);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.WineName.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterlevel")
                {
                    AbstractMasterLevel abstractMasterLevel = new MasterLevel();
                    var model = abstractMasterLevelServices.MasterLevel_All(pageParam, "", abstractMasterLevel);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "mastersuppliers")
                {
                    AbstractMasterSuppliers abstractMasterSuppliers = new MasterSuppliers();
                    var model = abstractMasterSuppliersServices.MasterSuppliers_All(pageParam, "", abstractMasterSuppliers);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                return items;
            }
            catch(Exception ex)
            {
                return items;
            }
        }

        public List<string> GetValues(string action = "")
        {
            List<string> values = new List<string>();
            PageParam pageParam = new PageParam();
            pageParam.Offset=0;
            pageParam.Limit=0;
            if(action=="LocationIds")
            {
                AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                var model = abstractMasterLocationsService.MasterLocations_All(pageParam, "", abstractMasterLocations);
                foreach(var master in model.Values)
                {
                    values.Add(master.LocationId.ToString());
                }
            }
            else if(action=="LocationName")
            {
                AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                var model = abstractMasterLocationsService.MasterLocations_All(pageParam, "", abstractMasterLocations);
                foreach(var master in model.Values)
                {
                    values.Add(master.LocationName.ToString());
                }
            }
            else if(action=="WineName")
            {
                AbstractMasterWines abstractMasterWines = new MasterWines();
                var model = abstractMasterWinesService.MasterWines_All(pageParam, "", abstractMasterWines);
                foreach(var master in model.Values)
                {
                    values.Add(master.WineName.ToString());
                }
            }

            return values;
        }

        public ActionResult AddDrink()
        {
            return View();
        }

        public ActionResult EditDrink()
        {
            return View();
        }

        public ActionResult ViewDrink()
        {
            return View();
        }
        #endregion
    }
}