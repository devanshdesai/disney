﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoDashboard.Entities.Contract
{
    public abstract class AbstractMasterServingSize
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
