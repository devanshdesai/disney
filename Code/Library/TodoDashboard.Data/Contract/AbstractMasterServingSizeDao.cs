﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDashboard.Common;
using TodoDashboard.Common.Paging;
using TodoDashboard.Entities.Contract;


namespace TodoDashboard.Data.Contract
{
    public abstract class AbstractMasterServingSizeDao
    {
        public abstract SuccessResult<AbstractMasterServingSize> MasterServingSize_Upsert(AbstractMasterServingSize abstractMasterServingSize);

        public abstract SuccessResult<AbstractMasterServingSize> MasterServingSize_Id(int Id);

        public abstract bool MasterServingSize_Delete(int Id);

        public abstract PagedList<AbstractMasterServingSize> MasterServingSize_All(PageParam pageParam, string search);
    }
}
